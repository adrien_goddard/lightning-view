<?php

namespace Lightning\ViewTest;

use Lightning\View\Page;

final class TagTest extends Base
{
	public function testEmpty()
	{
		echo _div();
		$this->expectOutputString('<div></div>');
	}
	
	public function testClosure()
	{
		echo _div(function($self) {
			$self->append(_span('test1'));
		})->exec(function($self) {
			$self->append(_span('test2'));
			$self->class('bg-primary');
		});
		$this->expectOutputString('<div class="bg-primary"><span>test1</span><span>test2</span></div>');
	}
	
	public function testClass()
	{
		echo _div()->class('text-center')->class('text-center');
		$this->expectOutputString('<div class="text-center"></div>');
	}

	public function testRemoveClass()
	{
		echo _div()->class('test')->removeClass('test');
		$this->expectOutputString('<div></div>');
	}
	
	public function testClasses()
	{
		$div = _div()->class('mt-1', 'mr-1 mb-1', ['ml-1']);
		
		// we check all the classes are separated
		$this->assertSame(['mt-1', 'mr-1', 'mb-1', 'ml-1'], $div->class);
		
		echo $div;
		$this->expectOutputString('<div class="mt-1 mr-1 mb-1 ml-1"></div>');
	}
	
	public function testAttributeFunction()
	{
		echo _div()->onclick('test');
		$this->expectOutputString('<div onclick="test"></div>');
		
		$this->expectException('PHPUnit\Framework\Error\Error');
		_div()->dataToggle(1, 2);
	}
	
	public function testAttributeGet()
	{
		$div = _div();
		$div->dataToggle = 'test';
		
		$this->assertSame('test', $div->dataToggle);
		
		$this->expectException('PHPUnit\Framework\Error\Error');
		$div->undefinedProperty;
	}
	
	public function testAttributeSet()
	{
		$div = _div();
		$div->class = 'test1';
		$div->onclick = 'test2';
		$div->dataToggle = 'test3';
		echo $div;
		
		$this->expectOutputString('<div class="test1" onclick="test2" data-toggle="test3"></div>');
		
		$this->expectException('PHPUnit\Framework\Error\Error');
		$div->undefinedProperty = 'test';
	}
	
	public function testAttributeUnset()
	{
		$div = _div()
				->class('test')
				->onclick('test')
				->dataToggle('test')
				->tooltip('test');
		
		unset($div->class);
		unset($div->onclick);
		unset($div->dataToggle);
		unset($div->tooltips);
		
		echo $div;
		$this->expectOutputString('<div></div>');
		
		$this->expectException('PHPUnit\Framework\Error\Error');
		unset($div->undefinedProperty);
	}
	
	public function testAttributeIsSet()
	{
		$div = _div();
		
		$this->assertTrue(isset($div->class));
		$this->assertTrue(isset($div->onclick));
		$this->assertTrue(isset($div->tooltips));
		$this->assertFalse(isset($div->dataToggle));
		$this->assertFalse(isset($div->notExistingAttribute));
		
		$div->dataToggle('test');
		
		$this->assertTrue(isset($div->dataToggle));
	}
	
	public function testJSAttribute()
	{
		$div = _div()->onclick(
			'test1',
			'test2'
		);
		$this->assertSame(['test1', 'test2'], $div->onclick);

		echo $div;
		$this->expectOutputString('<div onclick="test1;test2"></div>');
	}
	
	public function testHTMLEntities()
	{
		echo _div('<é×À>')->tooltip("<é×À>")->onclick("alert('<é×À>')");
		$this->expectOutputString('<div onclick="alert(&#039;&lt;&eacute;&times;&Agrave;&gt;&#039;)" class="has-tooltip" data-html="true" title="&lt;&eacute;&times;&Agrave;&gt;">&lt;&eacute;&times;&Agrave;&gt;</div>');
	}
	
	public function testAdditionalAttributeValue()
	{
		// we set an additional attribute
		$button = _button()->type('submit');
		
		$this->assertSame('submit', $button->type);
	}
	
	public function testDataAttribute()
	{
		$this->assertSame('test', _div()->dataToggle('test')->dataToggle);
	}
	
	public function testTooltip()
	{
		echo _div()->tooltip('test1')->tooltip('test2', 'test3');
		
		$this->expectOutputString($this->sanitize('
			<div class="has-tooltip" data-html="true" title="
				test1<br />
				test2<br />
				test3
			"></div>
		'));
	}
	
	public function testTooltipJS()
	{
		$page = new Page(
			_div()->tooltip('test')
		);
		
		$this->assertStringContainsString("$('.has-tooltip').tooltip();", $page->__toString());
	}
	
	public function testReadingTooltips()
	{
		$div = _div()->tooltip(1)->tooltip(2, 3);
		
		$this->assertCount(3, $div->tooltips);
	}
	
	public function testReplacingTooltips()
	{
		$div = _div()->tooltip(1);
		$div->tooltips = 2;
		
		$this->assertSame([2], $div->tooltips);
	}
	
	public function testCallUndefinedMethod()
	{
		$this->expectException('PHPUnit\Framework\Error\Error');
		_div()->undefinedMethod();
	}
}
