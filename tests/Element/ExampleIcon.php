<?php

namespace Lightning\ViewTest\Element;

use Lightning\View\Elements\Icon;

class ExampleIcon extends Icon
{
	static $resource = 'https://unpkg.com/ionicons@4.5.5/dist/ionicons.js';
	
	public static function getFillableAttributes(): array
	{
		return ['name' => null];
	}
	
	public function __construct(string $name)
	{
		parent::__construct('ion-icon');
		$this->name($name);
	}
}
