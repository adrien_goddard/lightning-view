<?php

namespace Lightning\ViewTest;

use Lightning\View\Page;

final class TagsTest extends Base
{
	public function testTemplatingFunctions()
	{
		echo implode([
			_p(),
			_small(),
			_mark(),
			_del(),
			_s(),
			_ins(),
			_u(),
			_strong(),
			_em(),
			_sup(),
			_sub(),
		]);
		
		$this->expectOutputString($this->sanitize('
			<p></p>
			<small></small>
			<mark></mark>
			<del></del>
			<s></s>
			<ins></ins>
			<u></u>
			<strong></strong>
			<em></em>
			<sup></sup>
			<sub></sub>
		'));
	}
	
	public function testA()
	{
		echo _a()->href('/')->blank();
		$this->expectOutputString('<a href="/" target="_blank"></a>');
	}
	
	public function testBr()
	{
		echo _br();
		$this->expectOutputString('<br />');
	}
	
	public function testHeadings()
	{
		echo _h1(_h2(_h3(_h4(_h5(_h6('test'))))));
		$this->expectOutputString('<h1><h2><h3><h4><h5><h6>test</h6></h5></h4></h3></h2></h1>');
	}
	
	public function testImg()
	{
		echo _img('/test')->alt('test');
		
		$this->expectOutputString('<img src="/test" alt="test" />');
	}
	
	public function testInput()
	{
		echo implode([
			_input(),
			_input()->name('test1'),
			_input()->name('test2')->value('test3'),
			_input()->readonly(),
			_input()->readonly(true),
			_input()->readonly(false),
			_input()->defaultValue('test4'),
			_input()->size('sm'),
		]);
		
		$this->expectOutputString($this->sanitize('
			<input class="form-control" type="text" />
			<input name="test1" class="form-control" type="text" />
			<input name="test2" value="test3" class="form-control" type="text" />
			<input readonly class="form-control" type="text" />
			<input readonly class="form-control" type="text" />
			<input class="form-control" type="text" />
			<input class="form-control" value="test4" type="text" />
			<input class="form-control form-control-sm" type="text" />
		'));
	}

	public function testTextarea()
	{
		echo _textarea();
		echo _textarea('test1');
		echo _textarea()->value('test2');
		
		$this->expectOutputString($this->sanitize('
			<textarea class="form-control"></textarea>
			<textarea class="form-control">test1</textarea>
			<textarea class="form-control">test2</textarea>
		'));
	}
	
	public function testButtonInHeading()
	{
		echo _h1(_button()) . _h3(_button());
		
		$this->expectOutputString($this->sanitize('
			<h1 class="d-flex align-items-center">
				<button class="btn btn-primary" type="button"></button>
			</h1>
			<h3 class="d-flex align-items-center">
				<button class="btn btn-primary btn-sm" type="button"></button>
			</h3>
		'));
	}
	
	public function testSubmitButton()
	{
		echo _submitbutton('test');
		echo _submitbutton('test')->name('test1');
		echo _submitbutton('test')->name('test1')->value('test2');
		
		$this->expectOutputString($this->sanitize('
			<button class="btn btn-primary" type="submit">test</button>
			<button class="btn btn-primary" type="submit" name="test1" value="test1">test</button>
			<button class="btn btn-primary" type="submit" name="test1" value="test2">test</button>
		'));
	}
	
	public function testContainers()
	{
		echo _container('test') . _fluidcontainer('test');
		
		$this->expectOutputString($this->sanitize('
			<div class="container">test</div>
			<div class="container-fluid">test</div>
		'));
	}
	
	public function testForm()
	{
		echo _form(
			_input()->name('test'),
			_submitbutton('send')
		);
		
		$this->expectOutputString($this->sanitize('
			<form method="post" enctype="multipart/form-data">
				<input name="test" class="form-control" type="text" />
				<button class="btn btn-primary" type="submit">send</button>
			</form>
		'));
	}
	
	public function testTable()
	{
		echo _table()->header(
			_thead(_tr(_th(1)))
		)->body(
			_tbody(_tr(_td(2)))
		)->footer(
			_tfoot(_tr(_th(3)))
		);
		
		$this->expectOutputString($this->sanitize('
			<table class="table">
				<thead>
					<tr>
						<th>1</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>2</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<th>3</th>
					</tr>
				</tfoot>
			</table>
		'));
	}
	
	public function testTableResponsive()
	{
		echo _table()->responsive('sm');
		
		$this->expectOutputString($this->sanitize('
			<div class="table-responsive-sm">
				<table class="table"></table>
			</div>
		'));
	}
	
	public function testTableAutoRowGroup()
	{
		echo _table()->header(
			_tr(_th(1)),
			_tr(_th(2))
		)->body(
			_tr(_td(3)),
			_tr(_td(4))
		)->footer(
			_tr(_th(5)),
			_tr(_th(6))
		);
		
		$this->expectOutputString($this->sanitize('
			<table class="table">
				<thead>
					<tr>
						<th>1</th>
					</tr>
					<tr>
						<th>2</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>3</td>
					</tr>
					<tr>
						<td>4</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<th>5</th>
					</tr>
					<tr>
						<th>6</th>
					</tr>
				</tfoot>
			</table>
		'));
	}
	
	public function testTableAutoRow()
	{
		echo _table()->header(
			_th(1),
			_th(2)
		)->body(
			_td(3),
			_td(4)
		)->footer(
			_th(5),
			_th(6)
		);
		
		$this->expectOutputString($this->sanitize('
			<table class="table">
				<thead>
					<tr>
						<th>1</th>
						<th>2</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>3</td>
						<td>4</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<th>5</th>
						<th>6</th>
					</tr>
				</tfoot>
			</table>
		'));
	}
	
	public function testTableAutoCell()
	{
		echo _table()->header(
			1
		)->body(
			2
		)->footer(
			3
		);
		
		$this->expectOutputString($this->sanitize('
			<table class="table">
				<thead>
					<tr>
						<th>1</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>2</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<th>3</th>
					</tr>
				</tfoot>
			</table>
		'));
	}
	
	public function testTableAppend()
	{
		echo _table()
			->append(1, 2)
			->append(function ($table) {
				$table->append(3, 4);
				$table->append(5, 6);
			});
		
		$this->expectOutputString($this->sanitize('
			<table class="table">
				<tbody>
					<tr>
						<td>1</td>
						<td>2</td>
					</tr>
					<tr>
						<td>3</td>
						<td>4</td>
					</tr>
					<tr>
						<td>5</td>
						<td>6</td>
					</tr>
				</tbody>
			</table>
		'));
	}

	public function testTableAppendArray()
	{
		echo _table()->append([
			'cell 1',
			'cell 2',
		]);

		$this->expectOutputString($this->sanitize('
			<table class="table">
				<tbody>
					<tr>
						<td>cell 1</td>
						<td>cell 2</td>
					</tr>
				</tbody>
			</table>
		'));
	}
	
	public function testTablePrepend()
	{
		echo _table()->prepend(
			_tr(5),
			_tr(6)
		)->prepend(
			_tbody(
				_tr(3),
				_tr(4)
			)
		)->prepend(
			_tr(1),
			_tr(2)
		);
		
		$this->expectOutputString($this->sanitize('
			<table class="table">
				<tbody>
					<tr>
						<td>1</td>
					</tr>
					<tr>
						<td>2</td>
					</tr>
					<tr>
						<td>3</td>
					</tr>
					<tr>
						<td>4</td>
					</tr>
				</tbody>
				<tbody>
					<tr>
						<td>5</td>
					</tr>
					<tr>
						<td>6</td>
					</tr>
				</tbody>
			</table>
		'));
	}
	
	public function testTableClasses()
	{
		echo _table()->header(
			_thead(
				_tr(
					_th(1)->class('test1')
				)->class('test2')
			)->class('test3')
		)->body(
			_tbody(
				_tr(
					_td(2)->class('test4')
				)->class('test5')
			)->class('test6')
		)->footer(
			_tfoot(
				_tr(
					_th(3)->class('test7')
				)->class('test8')
			)->class('test9')
		);
		
		$this->expectOutputString($this->sanitize('
			<table class="table">
				<thead class="test3">
					<tr class="test2">
						<th class="test1">1</th>
					</tr>
				</thead>
				<tbody class="test6">
					<tr class="test5">
						<td class="test4">2</td>
					</tr>
				</tbody>
				<tfoot class="test9">
					<tr class="test8">
						<th class="test7">3</th>
					</tr>
				</tfoot>
			</table>
		'));
	}
	
	public function testTableSkinnyColumn()
	{
		echo _table()->header(
			1,
			2,
			_skinnycolumn()
		)->body(
			_tr(
				3,
				4,
				5
			)
		);
		
		$this->expectOutputString($this->sanitize('
			<table class="table">
				<thead>
					<tr>
						<th>1</th>
						<th>2</th>
						<th class="skinny-column"></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>3</td>
						<td>4</td>
						<td class="skinny-column">5</td>
					</tr>
				</tbody>
			</table>
		'));
	}
}
