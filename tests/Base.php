<?php

namespace Lightning\ViewTest;

use PHPUnit\Framework\TestCase;

class Base extends TestCase
{
	public static function setUpBeforeClass(): void
	{
		require_once __DIR__ . '/../src/templatingFunctions.php';
	}
	
	public function sanitize($html)
	{
		return str_replace(["\r", "\n", chr(9)], '', $html);
	}
}
