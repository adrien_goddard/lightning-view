<?php

namespace Lightning\ViewTest;

final class HasParentTest extends Base
{
	public function testParent()
	{
		$div1 = _div();
		$div2 = _div($div1);
		$div3 = _div($div2);
		$this->assertSame($div3, $div1->getParent()->getParent());
	}
	
	public function testSelectFirstParent()
	{
		$select = _select();
		$div = _div($select);
		$this->assertSame($div, $select->input()->getRootParent());
	}
}
