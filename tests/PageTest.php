<?php

namespace Lightning\ViewTest;

use Lightning\View\Page;

final class PageTest extends Base
{
	public function testPage()
	{
		$page = new Page;
		$page->append(_div('test'));
		echo $page;
		
		$this->expectOutputString($this->sanitize('
			<!doctype html>
			<html lang="en">
				<head>
					<meta charset="utf-8" />
					<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
				</head>
				<body>
					<div>test</div>
				</body>
			</html>
		'));
	}
}
