<?php

namespace Lightning\ViewTest;

use Lightning\View\Page;
use Lightning\View\Elements\CustomCheckbox;
use Lightning\View\Elements\Toast;
use Lightning\ViewTest\Element\ExampleIcon;

final class ElementsTest extends Base
{
	public function testDefaultButton()
	{
		echo _button('test');
		$this->expectOutputString('<button class="btn btn-primary" type="button">test</button>');
	}
	
	public function testCustomButton()
	{
		echo _button()->color('secondary')->size('sm')->onclick('test');
		$this->expectOutputString('<button class="btn btn-secondary btn-sm" onclick="test" type="button"></button>');
	}
	
	public function testHrefButton()
	{
		echo _button()->color('secondary')->size('sm')->href('/');
		$this->expectOutputString('<a class="btn btn-secondary btn-sm" href="/"></a>');
	}
	
	public function testColumns()
	{
		echo _row(
			_col('test1'),
			_col('test2')->size(8)
		);
		
		$this->expectOutputString($this->sanitize('
			<div class="row">
				<div class="col">test1</div>
				<div class="col-8">test2</div>
			</div>
		'));
		
		$this->expectException('PHPUnit\Framework\Error\Error');
		_div(_col());
	}
	
	public function testModal()
	{
		echo _button('button')
			->modal()
			->header('header')
			->body('content')
			->footer(_closebutton('close'));
		
		$this->expectOutputString($this->sanitize('
			<button class="btn btn-primary" data-toggle="modal" data-target="#modal1" type="button">button</button>
			<div class="modal fade" id="modal1">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">header</h5>
							<button class="close" data-dismiss="modal" type="button">&times;</button>
						</div>
						<div class="modal-body">
							content
						</div>
						<div class="modal-footer">
							<button class="btn btn-primary" data-dismiss="modal" type="button">close</button>
						</div>
					</div>
				</div>
			</div>
		'));
	}
	
	public function testModalWithEmptyHeaderAndFooter()
	{
		echo _button('button')
			->modal('content')
			->id('modal');
		
		$this->expectOutputString($this->sanitize('
			<button class="btn btn-primary" data-toggle="modal" data-target="#modal" type="button">button</button>
			<div class="modal fade" id="modal">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button class="close" data-dismiss="modal" type="button">&times;</button>
						</div>
						<div class="modal-body">
							content
						</div>
					</div>
				</div>
			</div>
		'));
	}
	
	public function testModaLastInButtonGroup()
	{
		echo _buttongroup(
			_button('test1'),
			_button('test2')->modal()->id('modal')
		);

		$this->expectOutputString($this->sanitize('
			<div class="btn-group">
				<button class="btn btn-primary" type="button">test1</button>
				<div class="modal fade" id="modal">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button class="close" data-dismiss="modal" type="button">&times;</button>
							</div>
							<div class="modal-body"></div>
						</div>
					</div>
				</div>
				<button class="btn btn-primary" data-toggle="modal" data-target="#modal" type="button">test2</button>
			</div>
		'));
	}

	public function testModalWithOtherHeadingSize()
	{
		echo _button()
			->modal()
			->id('modal')
			->header(_h4('title'))
			->body('content');
		
		$this->expectOutputString($this->sanitize('
			<button class="btn btn-primary" data-toggle="modal" data-target="#modal" type="button"></button>
			<div class="modal fade" id="modal">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">title</h4>
							<button class="close" data-dismiss="modal" type="button">&times;</button>
						</div>
						<div class="modal-body">
							content
						</div>
					</div>
				</div>
			</div>
		'));
	}
	
	public function testListing()
	{
		echo _ul(
			_li('test1'),
			'test2'
		);
		echo _ol(
			'test'
		);
		
		$this->expectOutputString($this->sanitize('
			<ul>
				<li>test1</li>
				<li>test2</li>
			</ul>
			<ol>
				<li>test</li>
			</ol>
		'));
		
		$this->expectException('PHPUnit\Framework\Error\Error');
		_div(_li('test'));
	}
	
	public function testListGroup()
	{
		echo _listgroup(
			1,
			_li(2),
			_li(3)->class('bg-light')
		);
		
		$this->expectOutputString($this->sanitize('
			<ul class="list-group">
				<li class="list-group-item">1</li>
				<li class="list-group-item">2</li>
				<li class="bg-light list-group-item">3</li>
			</ul>
		'));
	}
	
	public function testInputGroup()
	{
		echo _inputgroup(
			_button('button1'),
			'text',
			_input(),
			_button('button2')
		);
		
		$this->expectOutputString($this->sanitize('
			<div class="input-group">
				<div class="input-group-prepend">
					<button class="btn btn-primary" type="button">button1</button>
				</div>
				<div class="input-group-append">
					<span class="input-group-text">text</span>
				</div>
				<input class="form-control" type="text" />
				<div class="input-group-append">
					<button class="btn btn-primary" type="button">button2</button>
				</div>
			</div>
		'));
		
		$this->expectException('PHPUnit\Framework\Error\Error');
		_inputgroup(_div());
	}
	
	public function testButtonGroup()
	{
		echo _buttongroup(
			_button('button1'),
			_button('button2'),
			_div()->class('btn')
		);
		
		$this->expectOutputString($this->sanitize('
			<div class="btn-group">
				<button class="btn btn-primary" type="button">button1</button>
				<button class="btn btn-primary" type="button">button2</button>
				<div class="btn"></div>
			</div>
		'));
		
		$this->expectException('PHPUnit\Framework\Error\Error');
		_buttongroup(_div());
	}
	
	public function testNavbar()
	{
		$navbarMenu = _navbarcollapse(
			_navbarnav(
				_a('link1')->href('/1')->class('active'),
				_a('link2')->href('/2')
			),
			_form(
				_inputgroup(
					_input()->type('search'),
					_submitbutton('send')
				)
			)
		);
		
		echo _navbar(
			_navbarbrand('brand'),
			$navbarMenu->getToggler(),
			$navbarMenu
		);
		
		$this->expectOutputString($this->sanitize('
			<nav class="navbar bg-primary navbar-expand-lg navbar-dark">
				<a class="navbar-brand" href="/">brand</a>
				<button class="btn navbar-toggler btn-primary" data-toggle="collapse" data-target="#toggler_target_1" type="button">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="toggler_target_1">
					<ul class="navbar-nav">
						<li class="nav-item">
							<a href="/1" class="active nav-link">link1</a>
						</li>
						<li class="nav-item">
							<a href="/2" class="nav-link">link2</a>
						</li>
					</ul>
					<form class="form-inline" method="post" enctype="multipart/form-data">
						<div class="input-group">
							<input type="search" class="form-control" />
							<div class="input-group-append">
								<button class="btn btn-primary" type="submit">send</button>
							</div>
						</div>
					</form>
				</div>
			</nav>
		'));
	}
	
	public function testDropdown()
	{
		echo _dropdown(
			'text',
			_a('link1'),
			_a('link2')->class('active'),
			_button('button'),
			_hr(),
			_h6('header')
		)->toggler('button content');
		
		$this->expectOutputString($this->sanitize('
			<div class="dropdown d-inline-block">
				<button class="btn dropdown-toggle btn-light" data-toggle="dropdown" type="button">button content </button>
				<div class="dropdown-menu">
					<span class="dropdown-item-text">text</span>
					<a class="dropdown-item">link1</a>
					<a class="active dropdown-item">link2</a>
					<button class="dropdown-item" type="button">button</button>
					<hr class="dropdown-divider" />
					<h6 class="dropdown-header">header</h6>
				</div>
			</div>
		'));
	}
	
	public function testDropdownColorSize()
	{
		echo _button()
			->dropdown()
			->color('default')
			->size('lg');
		
		$this->expectOutputString($this->sanitize('
			<div class="dropdown d-inline-block">
				<button class="btn dropdown-toggle btn-default btn-lg" data-toggle="dropdown" type="button"></button>
				<div class="dropdown-menu"></div>
			</div>
		'));
	}
	
	public function testDropdownWithArrow()
	{
		echo _button()->dropdown()->enableArrow();
		
		$this->expectOutputString($this->sanitize('
			<div class="dropdown d-inline-block">
				<button class="btn dropdown-toggle btn-primary" data-toggle="dropdown" type="button"></button>
				<div class="dropdown-menu"></div>
			</div>
		'));
	}
	
	public function testDropdownWithoutArrow()
	{
		echo _button()->dropdown()->disableArrow();
		
		$this->expectOutputString($this->sanitize('
			<div class="dropdown d-inline-block">
				<button class="btn btn-primary" data-toggle="dropdown" type="button"></button>
				<div class="dropdown-menu"></div>
			</div>
		'));
	}
	
	public function testDropdownOpenOnClick()
	{
		echo _button()->dropdown()->openOnClick();
		
		$this->expectOutputString($this->sanitize('
			<div class="dropdown d-inline-block">
				<button class="btn dropdown-toggle btn-primary" data-toggle="dropdown" type="button"></button>
				<div class="dropdown-menu"></div>
			</div>
		'));
	}

	public function testDropdownOpenOnHover()
	{
		echo _button()->dropdown()->openOnHover();

		$this->expectOutputString($this->sanitize('
			<div class="dropdown open-on-hover d-inline-block">
				<button class="btn dropdown-toggle btn-primary" type="button"></button>
				<div class="dropdown-menu"></div>
			</div>
		'));
	}
	
	public function testDropdownInNavbarNav()
	{
		echo _navbarnav(
			_a('dropdown')->dropdown(
				_a('link1')->href('/1')
			)
		);
		
		$this->expectOutputString($this->sanitize('
			<ul class="navbar-nav">
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
						dropdown 
					</a>
					<div class="dropdown-menu">
						<a href="/1" class="dropdown-item">link1</a>
					</div>
				</li>
			</ul>
		'));
	}
	
	public function testDropdownInButtonGroup()
	{
		echo _buttongroup(
			_button('dropdown')->dropdown(
				_button('button1'),
				_button('button2')
			)
		);
		
		$this->expectOutputString($this->sanitize('
			<div class="btn-group">
				<div class="btn-group">
					<button class="btn dropdown-toggle btn-primary" data-toggle="dropdown" type="button">dropdown </button>
					<div class="dropdown-menu">
						<button class="dropdown-item" type="button">button1</button>
						<button class="dropdown-item" type="button">button2</button>
					</div>
				</div>
			</div>
		'));
	}
	
	public function testDropdownDisplayBlock()
	{
		echo _button()->dropdown()->class('d-block');
		
		$this->expectOutputString($this->sanitize('
			<div class="dropdown d-block">
				<button class="btn dropdown-toggle btn-block btn-primary" data-toggle="dropdown" type="button"></button>
				<div class="dropdown-menu"></div>
			</div>
		'));
	}
	
	public function testDropdownInInputGroup()
	{
		echo _inputgroup(
			_button('button1'),
			_button('dropdown1')->dropdown(
				_button('button2')
			),
			_input(),
			_button('button3'),
			_button('dropdown2')->dropdown(
				_button('button4')
			)
		);
		
		$this->expectOutputString($this->sanitize('
			<div class="input-group select">
				<div class="input-group-prepend">
					<button class="btn btn-primary" type="button">button1</button>
				</div>
				<div class="input-group-append">
					<button class="btn dropdown-toggle btn-primary" data-toggle="dropdown" type="button">dropdown1 </button>
					<div class="dropdown-menu">
						<button class="dropdown-item" type="button">button2</button>
					</div>
				</div>
				<input class="form-control" type="text" />
				<div class="input-group-append">
					<button class="btn btn-primary" type="button">button3</button>
				</div>
				<div class="input-group-append">
					<button class="btn dropdown-toggle btn-primary" data-toggle="dropdown" type="button">dropdown2 </button>
					<div class="dropdown-menu">
						<button class="dropdown-item" type="button">button4</button>
					</div>
				</div>
			</div>
		'));
	}
	
	public function testSelect()
	{
		echo _select([
			1 => 'test 1',
			3 => 'test 3',
			2 => 'test 2',
		])->name('test');
		
		$this->expectOutputString($this->sanitize('
			<div class="dropdown d-inline-block">
				<input type="hidden" name="test" value="1" />
				<button class="btn dropdown-toggle btn-light" data-toggle="dropdown" type="button">test 1 </button>
				<div class="dropdown-menu select-menu">
					<button class="dropdown-item" data-value="1" type="button">test 1</button>
					<button class="dropdown-item" data-value="3" type="button">test 3</button>
					<button class="dropdown-item" data-value="2" type="button">test 2</button>
				</div>
			</div>
		'));
	}
	
	public function testSelectWithEmptyValue()
	{
		echo _select([1 => '']);
		
		$this->expectOutputString($this->sanitize('
			<div class="dropdown d-inline-block">
				<input type="hidden" value="1" />
				<button class="btn dropdown-toggle btn-light" data-toggle="dropdown" type="button">&nbsp; </button>
				<div class="dropdown-menu select-menu">
					<button class="dropdown-item" data-value="1" type="button">&nbsp;</button>
				</div>
			</div>
		'));
	}
	
	public function testSelectWithCustomValue()
	{
		echo _select([1 => 'test 1', 2 => 'test 2'])->value(2);
		
		$this->expectOutputString($this->sanitize('
			<div class="dropdown d-inline-block">
				<input type="hidden" value="2" />
				<button class="btn dropdown-toggle btn-light" data-toggle="dropdown" type="button">test 2 </button>
				<div class="dropdown-menu select-menu">
					<button class="dropdown-item" data-value="1" type="button">test 1</button>
					<button class="dropdown-item" data-value="2" type="button">test 2</button>
				</div>
			</div>
		'));
	}
	
	public function testSelectWithDefaultValue()
	{
		echo _select([0, 1, 2])->defaultValue(1);
		
		$this->expectOutputString($this->sanitize('
			<div class="dropdown d-inline-block">
				<input type="hidden" value="1" />
				<button class="btn dropdown-toggle btn-light" data-toggle="dropdown" type="button">1 </button>
				<div class="dropdown-menu select-menu">
					<button class="dropdown-item" data-value="0" type="button">0</button>
					<button class="dropdown-item" data-value="1" type="button">1</button>
					<button class="dropdown-item" data-value="2" type="button">2</button>
				</div>
			</div>
		'));
	}
	
	public function testSelectWithTitle()
	{
		echo _select(['test'])->title('title');
		
		$this->expectOutputString($this->sanitize('
			<div class="input-group select">
				<div class="input-group-prepend">
					<span class="input-group-text">title</span>
				</div>
				<div class="input-group-append">
					<input type="hidden" value="0" />
					<button class="btn dropdown-toggle btn-light" data-toggle="dropdown" type="button">test </button>
					<div class="dropdown-menu dropdown-menu-right select-menu">
						<button class="dropdown-item" data-value="0" type="button">test</button>
					</div>
				</div>
			</div>
		'));
	}
	
	public function testSelectOpenOnHover()
	{
		echo _select(['test'])->title('title')->openOnHover();
		
		$this->expectOutputString($this->sanitize('
			<div class="input-group select">
				<div class="input-group-prepend">
					<span class="input-group-text">title</span>
				</div>
				<div class="open-on-hover input-group-append">
					<input type="hidden" value="0" />
					<button class="btn dropdown-toggle btn-light" type="button">test </button>
					<div class="dropdown-menu dropdown-menu-right select-menu">
						<button class="dropdown-item" data-value="0" type="button">test</button>
					</div>
				</div>
			</div>
		'));
	}
	
	public function testCard()
	{
		echo _card()
			->header('header')
			->body(
				'content',
				_h5('title')
			)
			->footer('footer');
		
		$this->expectOutputString($this->sanitize('
			<div class="card">
				<div class="card-header">header</div>
				<div class="card-body">
					content
					<h5 class="card-title">title</h5>
				</div>
				<div class="card-footer">footer</div>
			</div>
		'));
	}
	
	public function testCardWithoutHeaderAndFooter()
	{
		echo _card();
		
		$this->expectOutputString($this->sanitize('
			<div class="card">
				<div class="card-body"></div>
			</div>
		'));
	}
	
	public function testCardWithHeadingHeader()
	{
		echo _card()
				->header(_h5('title'));
		
		$this->expectOutputString($this->sanitize('
			<div class="card">
				<h5 class="card-header">title</h5>
				<div class="card-body"></div>
			</div>
		'));
	}
	
	public function testFileInput()
	{
		echo _fileinput('title')->name('test');
		
		$this->expectOutputString($this->sanitize('
			<div class="custom-file">
				<input type="file" class="custom-file-input" name="test" />
				<label class="custom-file-label">title</label>
			</div>
		'));
	}
	
	public function testFileButton()
	{
		echo _filebutton('test')
			->name('test2')
			->size('sm')
			->color('success')
			->addForm();
		
		$this->expectOutputString($this->sanitize('
			<label class="btn mb-0 btn-success btn-sm">
				<form method="post" enctype="multipart/form-data">
					<input type="file" class="d-none" name="test2" />
				</form>
				test
			</label>
		'));
	}

	public function testIconMargin()
	{
		echo _button(
			new ExampleIcon('checkmark'),
			'test'
		);
		
		$this->expectOutputString($this->sanitize('
			<button class="btn btn-primary" type="button">
				<ion-icon name="checkmark" class="mr-1"></ion-icon>
				test
			</button>
		'));
	}

	public function testAlert()
	{
		echo _alert('test');
		
		$this->expectOutputString($this->sanitize('
			<div class="alert alert-primary">test</div>
		'));
	}

	public function testToast()
	{
		echo _toast('test1')
			->header('header')
			->body('test2');
		
		$this->expectOutputString($this->sanitize('
			<div class="toast">
				<div class="toast-header">
					header
					<button class="ml-auto mb-1 pl-2 close" data-dismiss="toast" type="button">&times;</button>
				</div>
				<div class="toast-body">
					test1
					test2
				</div>
			</div>
		'));
	}

	public function testToastResource()
	{
		$page = new Page(_toast());
		$page->getHtml();
		
		$this->assertContains(Toast::$js, $page->js);
	}

	public function testFormGroup()
	{
		echo _formgroup(
			_label('label'),
			_input()
		);
		
		$this->expectOutputString($this->sanitize('
			<div class="form-group">
				<label>label</label>
				<input class="form-control" type="text" />
			</div>
		'));
	}

	public function testFormGroupWithCol()
	{
		echo _formgroup(
			_label()->class('col-md-4'),
			_col(
				_select()
			)->class('col-md-8')
		);
		
		$this->expectOutputString($this->sanitize('
			<div class="form-group row">
				<label class="col-md-4 col-form-label"></label>
				<div class="col-md-8 col">
					<div class="dropdown d-inline-block">
						<input type="hidden" />
						<button class="btn dropdown-toggle btn-light" data-toggle="dropdown" type="button"></button>
						<div class="dropdown-menu select-menu"></div>
					</div>
				</div>
			</div>
		'));
	}

	public function testFormGroupWithNullChild()
	{
		_formgroup(null);
		
		$this->assertTrue(true); // no error should be thrown
	}

	public function testBadge()
	{
		echo _badge('test');
		
		$this->expectOutputString($this->sanitize('
			<span class="badge badge-primary">test</span>
		'));
	}
	
	public function testCheckboxWithLabel()
	{
		CustomCheckbox::resetCounter();
		
		echo _checkbox()
			->name('input_name')
			->label('Checkbox label');
		
		$this->expectOutputString($this->sanitize('
			<div class="custom-control custom-checkbox">
				<input class="custom-control-input" name="input_name" id="checkbox_with_label_0" type="checkbox" />
				<label class="custom-control-label" for="checkbox_with_label_0">
					Checkbox label
				</label>
			</div>
		'));
	}
	
	public function testArrayTooltip()
	{
		echo _div()->tooltip(['a', 'b']);
		
		$this->expectOutputString($this->sanitize('
			<div class="has-tooltip" data-html="true" title="a<br />b"></div>
		'));
	}
	
	public function testCheckboxWithLabelCounter()
	{
		CustomCheckbox::resetCounter();
		
		echo _checkbox()->label('Label 1');
		echo _checkbox()->label('Label 2');
		
		$this->expectOutputString($this->sanitize('
			<div class="custom-control custom-checkbox">
				<input class="custom-control-input" id="checkbox_with_label_0" type="checkbox" />
				<label class="custom-control-label" for="checkbox_with_label_0">
					Label 1
				</label>
			</div>
			<div class="custom-control custom-checkbox">
				<input class="custom-control-input" id="checkbox_with_label_1" type="checkbox" />
				<label class="custom-control-label" for="checkbox_with_label_1">
					Label 2
				</label>
			</div>
		'));
	}
	
	public function testDropdownHasChildren()
	{
		$dropdown = _dropdown([]);
		
		$this->assertFalse($dropdown->hasChildren());
	}
	
	public function testModalInDropdown()
	{
		$modal = _button('button')->modal();
		
		echo _dropdown([$modal]);
		
		$this->expectOutputString($this->sanitize('
			<div class="dropdown d-inline-block">
				<button class="btn dropdown-toggle btn-light" data-toggle="dropdown" type="button"></button>
				<div class="dropdown-menu">
					<button class="dropdown-item" data-toggle="modal" data-target="#modal2" type="button">button</button>
					<div class="modal fade" id="modal2">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header"><button class="close" data-dismiss="modal" type="button">&times;</button></div>
								<div class="modal-body"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		'));
	}

	public function testToastDisableAutohide()
	{
		echo _toast()->body('Hello world !')->disableAutohide('false');
		
		$this->expectOutputString($this->sanitize('
			<div class="toast" data-autohide="false">
				<div class="toast-header">
					<button class="ml-auto mb-1 pl-2 close" data-dismiss="toast" type="button">&times;</button>
				</div>
				<div class="toast-body">Hello world !</div>
			</div>
		'));
	}
	
	public function testTableWithNullCell()
	{
		echo _table()->append('test1', null, 'test2');
		$this->expectOutputString($this->sanitize('
			<table class="table">
				<tbody>
					<tr>
						<td>test1</td>
						<td></td>
						<td>test2</td>
					</tr>
				</tbody>
			</table>
		'));
	}
}
