<?php

namespace Lightning\ViewTest;

final class HasChildrenTest extends Base
{
	public function testChildString()
	{
		echo _div('test');
		$this->expectOutputString('<div>test</div>');
	}
	
	public function testChildRawHTML()
	{
		echo _div(_html('<span>test</span>'));
		$this->expectOutputString('<div><span>test</span></div>');
	}
	
	public function testChildArray()
	{
		echo _div([_span('test1'), 'test2']);
		$this->expectOutputString('<div><span>test1</span>test2</div>');
	}
	
	public function testChildren()
	{
		echo _div(
			_span('test1'),
			_span('test2')
		);
		$this->expectOutputString('<div><span>test1</span><span>test2</span></div>');
	}

	public function testFirstChild()
	{
		$tag = _div();
		$this->assertNull($tag->getFirstChild());

		$tag = _div(
			_span('test1'),
			_span('test2'),
			_span('test3'),
			_span('test4')
		);
		echo $tag->getFirstChild();
		$this->expectOutputString('<span>test1</span>');
	}

	public function testLastChild()
	{
		$tag = _div();
		$this->assertNull($tag->getLastChild());

		$tag = _div(
			_span('test1'),
			_span('test2'),
			_span('test3'),
			_span('test4')
		);
		echo $tag->getLastChild();
		$this->expectOutputString('<span>test4</span>');
	}

	public function testIsLastChild()
	{
		$button1 = _button('test1');
		$button2 = _button('test2');
		$button3 = _button('test3');

		$buttonGroup = _buttongroup(
			$button1,
			$button2,
			$button3
		);
		$this->assertTrue($button3->isLastChild());
	}
	
	public function testContentAppendPrepend()
	{
		echo _div()->content(
			_div('test2'),
			_div('test3')
		)->append(
			_div('test4')
		)->prepend(
			_div('test1')
		);
		
		$this->expectOutputString($this->sanitize('
			<div>
				<div>test1</div>
				<div>test2</div>
				<div>test3</div>
				<div>test4</div>
			</div>
		'));
	}
	
	public function testChildrenContainer()
	{
		echo _card()->append('test');
		
		$this->expectOutputString($this->sanitize('
			<div class="card">
				<div class="card-body">test</div>
			</div>
		'));
	}
	
	public function testReplaceWith()
	{
		$parent = _div();
		$oldChild = _div();
		$newChild = _div();
		
		$parent->append($oldChild);
		$oldChild->replaceWith($newChild);
		
		$this->assertSame($parent, $newChild->getParent());
	}
}
