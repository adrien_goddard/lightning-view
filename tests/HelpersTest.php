<?php

namespace Lightning\ViewTest;

use PHPUnit\Framework\TestCase;

final class HelpersTest extends TestCase
{
	public function testInterpose()
	{
		$this->assertSame([1, ' ', 2, ' ', 3], interpose(' ', [1, 2, 3]));
	}
}
