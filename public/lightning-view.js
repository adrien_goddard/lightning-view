$(function () {
	// we change the open-on-hover menus to open-on-click when we are on a touch-only device
	if (!matchMedia('(pointer:fine)').matches) {
		let navItem = $('.nav-item.dropdown.open-on-hover');
		navItem.removeClass('open-on-hover');
		navItem.find('.nav-link').attr('href', '#').attr('data-toggle', 'dropdown');
	}

	// clicking on a select's button will change its value
	initializeClickOnSelectMenuButtons();
	
	// moves the modal at the end of the body to avoid CSS added to the modal's parents
	$('.modal').on('show.bs.modal', function (e) {
		$modal = $(this);
		
		if (!$modal.parent().is('body')) {
			id = 'modal_replace_' + $modal.attr('id');
			$modal.after($('<span>', {id: id}));
			
			$('body').append($modal);
			
			// if the modal has a submit, clicking on it will :
			// - close the modal first (so it gets back to it's original position)
			// - triggers the submit
			$modal.find("button[type='submit']")
				.attr('type', 'button')
				.click(function () {
					let submit = $(this);
					$(this).closest('.modal').modal('hide').on('hidden.bs.modal', function () {
						submit.attr('type', 'submit').click();
					});
				});
		}
	});
	// moves back the modal at it's original position
	$('.modal').on('hidden.bs.modal', function (e) {
		$modal = $(this);
		id = 'modal_replace_' + $modal.attr('id');
		$('#' + id).replaceWith($modal);
	});
});

function initializeClickOnSelectMenuButtons() {
	$('.select-menu button').off('click.lightning.view');
	
	$('.select-menu button').on('click.lightning.view', function () {
		// change the value of the input and trigger its change event
		let value = $(this).attr('data-value');
		let input = $(this).parent().siblings('input[type=hidden]');
		let previousValue = input.val();
		
		input.val(value);
		if (previousValue != value) {
			input.change();
		}
		
		// change the HTML of the toggler
		let html = $(this).html() + ' ';
		$(this).parent().siblings('button').html(html);
	});
}
