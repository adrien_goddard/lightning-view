<?php

use Lightning\View\Page;

require '../vendor/autoload.php';

$page = new Page;
$page->addResource('https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
$page->addResource('https://code.jquery.com/jquery-3.3.1.slim.min.js');
$page->addResource('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js');
$page->addResource('https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js');
$page->addResource('/public/lightning-view.css');
$page->addResource('/public/lightning-view.js');

$navbarMenu = _navbarcollapse(
	_navbarnav(
		_a('Link 1')->href('/1')->class('active'),
		_a('Link 2')->href('/2'),
		_a('Dropdown')->dropdown(
			_a('Link 3')->href('/3'),
			_a('Link 4')->href('/4')
		)
	)->class('mr-auto'),

	_form(
		_inputgroup(
			_input()->type('search')->name('search'),
			_submitbutton('Search')->color('outline-light')
		)
	)->class('my-2', 'my-lg-0')
);

$page->append(
	_navbar(
		_navbarbrand('Lightning View'),
		$navbarMenu->getToggler(),
		$navbarMenu
	)->class('mb-3'),
	_container(
		_h1('Hello world')->class('mb-4'),

		_row()->class('text-center mb-4')->content(
			_col('Row 1'),
			_col('Row 2')->size(6),
			_col('Row 3')
		),

		_div(
			_button('Button with tooltip')->tooltip('Tooltip content'),
			_button('Button with modal')
				->class('ml-4')
				->modal()
				->header('Modal title')
				->content('Modal content')
				->footer(_closebutton('Close')),
			_button('Button with dropdown')->dropdown(
				_a('Link 1')->href('/example/'),
				_a('Link 2')->href('/example/')->class('active'),
				_hr(),
				'Simple text',
				_a('Link 3')->href('/example/'),
				_a('Link 4')->href('/example/')
			)->class('ml-4'),
			_buttongroup(
				_button('Button'),
				_button('Group')->color('info'),
				_button('With dropdown')->color('secondary')->dropdown(
					_button('Button 1'),
					_button('Button 2')
				)
			)->class('ml-4')
		)->class('mb-4'),

		_inputgroup(
			_button('Dropdown')->dropdown(
				_button('Button 1'),
				_button('Button 2')
			),
			_input(),
			_button('Submit')->type('submit')
		)->class('mb-4'),

		_select(['t' => 'Top', 'b' => 'Bottom', 'l' => 'Left', 'r' => 'Right'])->name('position')->title('Select with title')
	)
);
	
echo $page;
