<?php

namespace Lightning\View;

use Lightning\View\Interfaces\ChildTag;
use Lightning\View\Interfaces\ParentTag;
use Lightning\View\Traits\HasParent;
use Lightning\View\Traits\HasChildren;

class TagWithFamily extends Tag implements ChildTag, ParentTag
{
	use HasParent;
	use HasChildren;
}
