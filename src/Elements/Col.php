<?php

namespace Lightning\View\Elements;

use Lightning\View\Interfaces\ChildTag;
use Lightning\View\TagWithFamily;
use Lightning\View\Traits\HasSize;

class Col extends TagWithFamily
{
	use HasSize;
	
	public function __construct($children = [])
	{
		parent::__construct();
		$this->append(...$children);
	}
	
	public function getHtml() : string
	{
		$this->addSizeClass('col', 'col');
		
		return parent::getHtml();
	}
	
	public function setParent($parent) : ChildTag
	{
		if (!$parent->hasClass('row')) {
			trigger_error('A Bootstrap "column" should always have a "row" as parent', E_USER_NOTICE);
		}
		
		return parent::setParent($parent);
	}
}
