<?php

namespace Lightning\View\Elements;

use Lightning\View\TagWithFamily;
use Lightning\View\Tags\Heading;

class Card extends TagWithFamily
{
	protected $body;
	protected $footer;
	protected $header;
	
	public function __construct($children = [])
	{
		parent::__construct();
		
		$this->header = _div()
			->class('card-header')
			->appendTo($this)
			->setOption('container', $this);
		
		$this->body = _div()
			->class('card-body')
			->appendTo($this)
			->setOption('container', $this)
			->setOption('child_parser', function ($body, $child) {
				return $body->options['container']->parseBodyChild($child);
			});
		
		$this->footer = _div()
			->class('card-footer')
			->appendTo($this)
			->setOption('container', $this);
		
		$this
			->class('card')
			->setChildrenContainer($this->body)
			->append(...$children);
	}
	
	public function getHtml() : string
	{
		if ($this->header->hasChildren()) {
			// if we only have one child, which is a heading
			if (
				count($this->header->getChildren()) == 1
				&& $this->header->getFirstChild() instanceof Heading
			) {
				$firstChild = $this->header->getFirstChild();
				$firstChild->class('card-header');
				
				// we can use it directly
				$this->header->replaceWith($firstChild);
			}
		} else {
			$this->header->remove();
		}
		
		// remove the footer if it is empty
		if (!$this->footer->hasChildren()) {
			$this->footer->remove();
		}
		
		return parent::getHtml();
	}
	
	public function parseBodyChild($child)
	{
		// automatically add the card-title class to the headings
		if ($child instanceof Heading) {
			$child->class('card-title');
		}
		
		return $child;
	}
}
