<?php

namespace Lightning\View\Elements;

use Lightning\View\Traits\HasHref;

class Button extends ButtonBase
{
	use HasHref;
	
	public function href($value): self
	{
		$this->setType('a');
		$this->type = '';
		
		return parent::href($value);
	}
	
	public function dropdown(...$children): Dropdown
	{
		return new Dropdown($children, $this);
	}
}
