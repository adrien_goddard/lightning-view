<?php

namespace Lightning\View\Elements;

use Lightning\View\Tags\Li;

class ListGroup extends Listing
{
	public function __construct($children = [])
	{
		parent::__construct('ul', $children);
		
		$this->class('list-group');
	}
	
	public function parseChild($child)
	{
		if ($child instanceof Li) {
			$child->class('list-group-item');
		} else {
			$child = _li($child)->class('list-group-item');
		}
		
		return parent::parseChild($child);
	}
}
