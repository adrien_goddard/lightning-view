<?php

namespace Lightning\View\Elements;

use Lightning\View\Elements\Modal;
use Lightning\View\Interfaces\ChildTag;
use Lightning\View\TagWithFamily;
use Lightning\View\Tags\A;
use Lightning\View\Tags\Button;
use Lightning\View\Tags\Heading;

class Dropdown extends TagWithFamily
{
	private $arrow = true;
	protected $menu;
	private $openOnHover = false;
	protected $toggler;
	// if the dropdown should be wrapped in a <div>
	public $wrapped = true;
	
	public static function getAliasProperties()
	{
		return ['toggler', 'menu'];
	}
	
	public function __construct($children = [], $toggler = null)
	{
		parent::__construct();
		
		if ($toggler === null) {
			$toggler = _button()->color('light');
		}
		$this->setToggler($toggler);
		
		$this->menu = _div()
			->class('dropdown-menu')
			->appendTo($this)
			->setOption('container', $this)
			->setOption('child_parser', function ($menu, $child) {
				return $menu->options['container']->parseMenuChild($child);
			});
		
		$this
			->class('dropdown')
			->setChildrenContainer($this->menu)
			->append(...$children);
	}
	
	public function getHtml() : string
	{
		if ($this->openOnHover) {
			if ($this->wrapped) {
				$this->class('open-on-hover');
			}
		} else {
			$this->toggler->dataToggle = 'dropdown';
			
			if ($this->toggler instanceof A) {
				$this->toggler->href('#');
			}
		}
		
		$this->prepareToggler();
		
		if ($this->wrapped) {
			if ($this->hasClass('d-block')) {
				$this->toggler->class('btn-block');
			} elseif (!array_intersect(['d-none', 'd-inline', 'd-table', 'd-table-row', 'd-table-cell', 'd-flex', 'd-inline-flex'], $this->class)) {
				// d-inline-block by default
				$this->class('d-inline-block');
			}
			
			return parent::getHtml();
		} else {
			return $this->getContent();
		}
	}
	
	protected function prepareToggler()
	{
		if ($this->arrow) {
			$this->toggler->class('dropdown-toggle');
		}
		
		if ($this->toggler->hasChildren()) {
			// we need to add a space to get the right space between the toggler content and the caret
			$this->toggler->append(' ');
		}
	}

	public function setToggler($toggler)
	{
		$toggler
			->appendTo($this)
			->setOption('container', $this);
		
		$this->toggler = $toggler;
	}
	
	public function setParent($parent) : ChildTag
	{
		if ($this->openOnHover && !$this->wrapped) {
			$parent->class('open-on-hover');
		}

		return parent::setParent($parent);
	}
	
	public function parseMenuChild($child)
	{
		if (is_string($child)) {
			$child = _span($child)->class('dropdown-item-text');
		} elseif ($child instanceof A) {
			$child->class('dropdown-item');
		} elseif ($child instanceof Button) {
			$child->color = '';
			$child->removeClass('btn');
			$child->class('dropdown-item');
		} elseif ($child instanceof Modal) {
			$child->button->color = '';
			$child->button->removeClass('btn');
			$child->button->class('dropdown-item');
		} elseif ($child instanceof Heading) {
			$child->class('dropdown-header');
		} elseif ($child->getType() == 'hr') {
			$child->class('dropdown-divider');
		}
		
		return $child;
	}

	/**
	 * Enable the arrow on the dropdown (this is the default behavior)
	 *
	 * @return self
	 */
	public function enableArrow() : self
	{
		$this->arrow = true;
		return $this;
	}

	/**
	 * Disable the arrow on the dropdown
	 *
	 * @return self
	 */
	public function disableArrow() : self
	{
		$this->arrow = false;
		return $this;
	}

	/**
	 * The dropdown menu will open on click (this is the default behavior)
	 *
	 * @return self
	 */
	public function openOnClick() : self
	{
		$this->openOnHover = false;
		return $this;
	}

	/**
	 * The dropdown menu will open on hover
	 *
	 * @return self
	 */
	public function openOnHover() : self
	{
		$this->openOnHover = true;
		return $this;
	}
}
