<?php

namespace Lightning\View\Elements;

use Lightning\View\Elements\Dropdown;
use Lightning\View\Elements\Icon;
use Lightning\View\Tag;
use Lightning\View\TagWithFamily;
use Lightning\View\Tags\Button;
use Lightning\View\Tags\InputBase;
use Lightning\View\Traits\HasSize;

class InputGroup extends TagWithFamily
{
	use HasSize;
	
	private $prepend = true;
	
	public function __construct($children = [])
	{
		parent::__construct();
		
		$this
			->class('input-group')
			->append(...$children);
	}
	
	public function getHtml() : string
	{
		$this->addSizeClass('input-group');
		
		return parent::getHtml();
	}

	public function parseChild($child)
	{
		if (
			!($child instanceof Tag)
			|| !$child->hasClass('input-group-prepend') && !$child->hasClass('input-group-append') && !$child instanceof InputBase
		) {
			if ($child instanceof Dropdown) {
				$child->wrapped = false;
				$this->class('select');
			} elseif ($child instanceof Icon || $child instanceof CustomCheckbox || is_string($child)) {
				$child = _span($child)->class('input-group-text');
			} elseif (!($child instanceof Button)) {
				trigger_error('An InputGroup should only have Input, Button or Dropdown as children', E_USER_NOTICE);
			}
			
			// we have to wrap the button in a <div> with a prepend or append class
			if ($this->prepend) {
				$child = _div($child)->class('input-group-prepend');
			} else {
				$child = _div($child)->class('input-group-append');
			}
		}
		
		if ($this->prepend) {
			// only the first element should have the "prepend" class
			$this->prepend = false;
		}
		
		return parent::parseChild($child);
	}
}
