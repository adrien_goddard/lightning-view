<?php

namespace Lightning\View\Elements;

use Lightning\View\TagWithFamily;
use Lightning\View\Tags\Li;

class Listing extends TagWithFamily
{
	public function __construct($type, $children = [])
	{
		parent::__construct($type);
		$this->append(...$children);
	}
	
	public function parseChild($child)
	{
		// automatically wrap the child in a <li>
		if (!($child instanceof Li)) {
			$child = new Li($child);
		}
		
		return parent::parseChild($child);
	}
}
