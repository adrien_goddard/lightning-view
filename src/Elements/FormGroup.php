<?php

namespace Lightning\View\Elements;

use Lightning\View\Tag;
use Lightning\View\TagWithFamily;

class FormGroup extends TagWithFamily
{
	public function __construct($children = [])
	{
		parent::__construct();
		
		$this
			->class('form-group')
			->append(...$children);
	}
	
	public function parseChild($child)
	{
		// if we find a Column or a tag with a col-* class
		if (
			$child instanceof Col
			|| (
				$child instanceof Tag
				&& array_filter($child->class, function ($class) {
					return substr($class, 0, 4) == 'col-';
				})
			)
		) {
			$this->class('row'); 
		}

		return parent::parseChild($child);
	}
}
