<?php

namespace Lightning\View\Elements;

use Lightning\View\TagWithFamily;
use Lightning\View\Traits\HasColor;

class Badge extends TagWithFamily
{
	use HasColor;

	public function __construct($children = [])
	{
		parent::__construct('span');
		
		$this
			->class('badge')
			->append(...$children);
	}
	
	public function getHtml() : string
	{
		$this->addColorClass('badge');
		
		return parent::getHtml();
	}
}
