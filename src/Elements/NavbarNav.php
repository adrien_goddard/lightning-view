<?php

namespace Lightning\View\Elements;

use Lightning\View\Tags\A;

class NavbarNav extends Listing
{
	public function __construct($children = [])
	{
		parent::__construct('ul', $children);
		
		$this->class('navbar-nav');
	}
	
	public function parseChild($child)
	{
		// automatically wrap the <a> in a <li> and add the right classes
		if ($child instanceof A) {
			$child->class('nav-link');
			$child = _li($child)->class('nav-item');
		} elseif ($child instanceof Dropdown) {
			$child->wrapped = false;
			$child->toggler()->class('nav-link');
			
			$child = _li($child)->class('nav-item', 'dropdown');
		}
		
		return parent::parseChild($child);
	}
}
