<?php

namespace Lightning\View\Elements;

use Lightning\View\TagWithFamily;

class FileInput extends TagWithFamily
{
	protected $input;
	protected $label;
	
	public static function getAliasProperties()
	{
		return ['input'];
	}
	
	public function __construct($children = [])
	{
		parent::__construct();
		
		$this->input = _input()
			->type('file')
			->class('custom-file-input')
			->appendTo($this)
			->setOption('container', $this);
		
		$this->label = _label()
			->class('custom-file-label')
			->appendTo($this)
			->setOption('container', $this);
		
		$this
			->class('custom-file')
			->setChildrenContainer($this->label)
			->append(...$children);
	}
}
