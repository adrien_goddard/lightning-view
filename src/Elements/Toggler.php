<?php

namespace Lightning\View\Elements;

class Toggler extends ButtonBase
{
	public function __construct($target, array $content = [])
	{
		parent::__construct();

		if (!isset($target->id)) {
			// a counter to make every collapsed div unique
			static $idCounter = 0;
			++$idCounter;
			
			$target->id = 'toggler_target_' . $idCounter;
		}

		$this->class('navbar-toggler')
			->dataToggle('collapse')
			->dataTarget('#' . $target->id)
			->append(...$content);
	}
}
