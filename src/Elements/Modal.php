<?php

namespace Lightning\View\Elements;

use Lightning\View\Tag;
use Lightning\View\TagWithFamily;
use Lightning\View\Tags\Button;
use Lightning\View\Tags\Heading;
use Lightning\View\Traits\HasSize;

class Modal extends TagWithFamily
{
	protected $body;
	public $button;
	public $buttonDetached = false;
	protected $content;
	protected $dialog;
	protected $header;
	protected $footer;

	use HasSize;

	public function __construct($children = [])
	{
		parent::__construct();
		
		$this->dialog = _div()
			->class('modal-dialog')
			->appendTo($this)
			->setOption('container', $this);
		
		$this->content = _div()
			->class('modal-content')
			->appendTo($this->dialog)
			->setOption('container', $this);
		
		$this->header = _div()
			->class('modal-header')
			->appendTo($this->content)
			->setOption('container', $this);
		$this->body = _div()
			->class('modal-body')
			->appendTo($this->content)
			->setOption('container', $this);
		$this->footer = _div()
			->class('modal-footer')
			->appendTo($this->content)
			->setOption('container', $this);
		
		$this
			->class('modal fade')
			->setChildrenContainer($this->body)
			->append(...$children);
	}
	
	public function getHtml() : string
	{
		$this->addSizeClass('modal');
		$this->prepareButton();
		$this->prepareHeader();
		$this->prepareFooter();
		
		if ($this->buttonDetached) {
			return parent::getHtml();
		}
		
		// if the modal is the last child of a buttongroup :
		// the button should be at last position so the rounded effect can apply
		if ($this->getParent() instanceof ButtonGroup && $this->isLastChild()) {
			return parent::getHtml() . $this->button;
		}

		return $this->button . parent::getHtml();
	}
	
	public function prepareButton()
	{
		if (!$this->id) {
			// a counter to make every modal id unique
			static $idCounter = 0;
			$idCounter++;

			$this->id('modal' . $idCounter);
		}
		
		$this->button
			->dataToggle('modal')
			->dataTarget('#' . $this->id);
	}
	
	public function prepareHeader()
	{
		// adding the header
		if (
			// if we only have one child, which is a heading
			count($this->header->getChildren()) == 1
			&& $this->header->getFirstChild() instanceof Heading
		) {
			$this->header->getFirstChild()->class('modal-title');
		} elseif ($this->header->hasChildren()) {
			// automatically wrap the header content in a h5
			$title = _h5(...$this->header->getChildren())->class('modal-title');
			$this->header->content($title);
		}

		// add the close button
		(new Button())
			->class('close')
			->dataDismiss('modal')
			->append('×')
			->appendTo($this->header);
	}
	
	public function prepareFooter()
	{
		// remove the footer if it is empty
		if (!$this->footer->hasChildren()) {
			$this->footer->remove();
		}
	}

	protected function getSizeContainer(): Tag
	{
		return $this->dialog;
	}

	public function detachButton(): Tag
	{
		$this->buttonDetached = true;
		
		return $this->button;
	}
}
