<?php

namespace Lightning\View\Elements;

use Lightning\View\Interfaces\ChildTag;
use Lightning\View\Interfaces\ParentTag;
use Lightning\View\Traits\HasChildren;
use Lightning\View\Traits\HasParent;

class Tabs implements ChildTag, ParentTag
{
	use HasChildren;
	use HasParent;
	
	public function __construct($children = [])
	{
		$this->append(...$children);
	}
	
	public function __toString() : string
	{
		return $this->getList() . $this->getPanes();
	}

	public function getList()
	{
		$return = _div()->class('list-group');

		foreach ($this->getChildren() as $key => $pane) {
			$pane->a
				->class('list-group-item list-group-item-action')
				->dataToggle('list')
				->href('#tab' . $key);

			$return->append($pane->a);
		}

		return $return;
	}

	public function getNav()
	{
		$return = _div()->class('nav nav-tabs');

		foreach ($this->getChildren() as $key => $pane) {
			$pane->a
				->class('nav-item nav-link')
				->dataToggle('tab')
				->href('#tab' . $key);

			$return->append($pane->a);
		}

		return $return;
	}

	public function getPanes()
	{
		$return = _div()->class('tab-content');

		foreach ($this->getChildren() as $key => $pane) {
			$pane->id('tab' . $key);

			if ($pane->a->hasClass('active')) {
				$pane->class('show active');
			}

			$return->append($pane);
		}

		return $return;
	}
}
