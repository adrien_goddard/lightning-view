<?php

namespace Lightning\View\Elements;

use Lightning\View\TagWithFamily;

class Pane extends TagWithFamily
{
	public $a;

	public function __construct($children, $a)
	{
		parent::__construct();
		
		$this->a = $a;

		$this->append(...$children)->class('tab-pane fade');
	}
}
