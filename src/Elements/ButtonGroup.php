<?php

namespace Lightning\View\Elements;

use Lightning\View\Tag;
use Lightning\View\TagWithFamily;
use Lightning\View\Elements\Modal;
use Lightning\View\Traits\HasSize;

class ButtonGroup extends TagWithFamily
{
	use HasSize;
	
	public function __construct($children = [])
	{
		parent::__construct();
		
		$this
			->class('btn-group')
			->append(...$children);
	}
	
	public function getHtml(): string
	{
		$this->addSizeClass('btn-group');
		
		return parent::getHtml();
	}
	
	public function parseChild($child)
	{
		if ($child instanceof Dropdown) {
			// we have to wrap the ButtonGroup in a div.btn-group to corretcly apply the rounded corners
			$buttonGroup = _div()->class('btn-group');
			$buttonGroup->append($child);
			$child->wrapped = false;

			return parent::parseChild($buttonGroup);
		} elseif (
			$child !== null
			&& !($child instanceof Modal)
			&& (!($child instanceof Tag) || !$child->hasClass('btn'))
		) {
			trigger_error('A ButtonGroup should only have Button or Dropdown or Tag with btn class as children', E_USER_NOTICE);
		}
		
		return parent::parseChild($child);
	}
}
