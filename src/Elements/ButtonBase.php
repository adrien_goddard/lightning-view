<?php

namespace Lightning\View\Elements;

use Lightning\View\Tags\Button;
use Lightning\View\Traits\HasColor;
use Lightning\View\Traits\HasSize;

class ButtonBase extends Button
{
	use HasColor;
	use HasSize;
	
	public function __construct($children = [])
	{
		parent::__construct($children);
		$this->class('btn');
	}
	
	public function getHtml() : string
	{
		$this->addColorClass('btn');
		$this->addSizeClass('btn');
		
		Icon::addMargin($this);
		
		return parent::getHtml();
	}
	
	public function modal(...$children): Modal
	{
		$modal = new Modal($children);
		
		// attaching this button to the modal
		$modal->button = $this;
		
		return $modal;
	}
}
