<?php

namespace Lightning\View\Elements;

use Lightning\View\Tags\Label;
use Lightning\View\TagWithFamily;

class CustomCheckbox extends TagWithFamily
{
	protected static $counter = 0;
	
	public $checkbox;
	public $label;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->checkbox = new Checkbox;
		$this->label = new Label;
		
		$this->checkbox->options['container'] = $this;
		$this->label->options['container'] = $this;
		
		$this->append($this->checkbox, $this->label);
		
		$this->class('custom-control custom-checkbox');
		$this->checkbox->class('custom-control-input');
		$this->label->class('custom-control-label');
		
		$this->options['method_proxies'] = [
			$this->checkbox,
			$this->label,
		];
	}
	
	public function getHtml(): string
	{
		$this->prepareCheckbox();
		$this->prepareLabel();
		
		return parent::getHtml();
	}
	
	private function prepareCheckbox()
	{
		if (!$this->checkbox->id) {
			// we need an id for the label
			$this->checkbox->id = 'checkbox_with_label_' . static::$counter;
			static::$counter++;
		}
	}
	
	private function prepareLabel()
	{
		$this->label->for($this->checkbox->id);
	}
	
	public static function resetCounter()
	{
		static::$counter = 0;
	}
}
