<?php

namespace Lightning\View\Elements;

use Lightning\View\TagWithFamily;
use Lightning\View\Traits\HasColor;
use Lightning\View\Traits\HasSize;

class FileButton extends TagWithFamily
{
	use HasColor;
	use HasSize;
	
	protected $input;
	
	public static function getAliasProperties()
	{
		return ['input'];
	}
	
	public function __construct($children = [])
	{
		parent::__construct('label');
		$this->append(...$children)->class('btn mb-0');
		
		$this->input = _input()
			->type('file')
			->class('d-none')
			->prependTo($this);
	}
	
	public function getHtml() : string
	{
		$this->addColorClass('btn');
		$this->addSizeClass('btn');
		
		Icon::addMargin($this);
		
		return parent::getHtml();
	}
	
	public function addForm()
	{
		$this->input->wrapIn(_form());
		
		return $this;
	}
}
