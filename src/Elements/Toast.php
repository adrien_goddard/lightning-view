<?php

namespace Lightning\View\Elements;

use Lightning\View\Page;
use Lightning\View\TagWithFamily;
use Lightning\View\Tags\Button;

class Toast extends TagWithFamily
{
	static $js = "$('.toast').toast({'delay': 4000}).toast('show');";
	
	protected $header;
	protected $body;
	
	public function __construct($children = [])
	{
		parent::__construct();
		
		$this->header = _div()
			->class('toast-header')
			->appendTo($this)
			->setOption('container', $this);
		$this->body = _div()
			->class('toast-body')
			->appendTo($this)
			->setOption('container', $this);
		
		$this
			->class('toast')
			->setChildrenContainer($this->body)
			->append(...$children);
	}
	
	public function getHtml() : string
	{
		$firstParent = $this->getRootParent();
		if ($firstParent instanceof Page) {
			$firstParent->js['toast'] = self::$js;
		}
		
		// add the close button
		(new Button())
			->class('ml-auto mb-1 pl-2 close')
			->dataDismiss('toast')
			->append('×')
			->appendTo($this->header);
		
		return parent::getHtml();
	}
	
	public function disableAutohide()
	{
		$this->dataAutohide('false');
		
		return $this;
	}
}
