<?php

namespace Lightning\View\Elements;

use Lightning\View\Page;
use Lightning\View\Tag;
use Lightning\View\TagWithFamily;
use Lightning\View\Tags\Form;
use Lightning\View\Traits\HasColor;
use Lightning\View\Traits\HasSize;

abstract class Icon extends TagWithFamily
{
	use HasColor;
	use HasSize;
	
	static $resource = null;
	
	public function getHtml(): string
	{
		$firstParent = $this->getRootParent();
		if ($firstParent instanceof Page) {
			if (static::$resource !== null) {
				$firstParent->addResource(static::$resource);
			}
		}

		$this->addColorClass('text', null);
		$this->addSizeClass('icon');
		
		return parent::getHtml();
	}
	
	public static function addMargin($parent)
	{
		// if we have more than one children
		if (count($parent->getChildren()) > 1) {
			$firstVisibleChild = $parent->getFirstChild(function ($child) {
				// not an object : there is no way to know if it is visible or not, so we presume it is visible
				if (!($child instanceof Tag)) {
					return true;
				}

				if ($child->hasClass('d-none')) {
					return false;
				}
				
				// form with only d-none children
				if ($child instanceof Form && !$child->getFirstChild(function ($child) {
					return !($child instanceof Tag) || !$child->hasClass('d-none');
				})) {
					return false;
				}
				
				return true;
			});
			
			// if the first visible child is an Icon
			if ($firstVisibleChild instanceof self) {
				if ($parent->hasClass('list-group-item-action')) {
					$firstVisibleChild->class('mr-2');
				} else {
					$firstVisibleChild->class('mr-1');
				}
			}
		}
	}
}
