<?php

namespace Lightning\View\Elements;

use Lightning\View\TagWithFamily;
use Lightning\View\Traits\HasColor;

class Navbar extends TagWithFamily
{
	use HasColor;
	
	public $bg;
	public $expand;
	
	public function __construct($children = [])
	{
		parent::__construct('nav');
		
		$this
				->class('navbar')
				->append(...$children);
	}
	
	public function getHtml() : string
	{
		// default values
		if (!isset($this->bg)) {
			$this->bg = 'primary';
		}
		if (!isset($this->expand)) {
			$this->expand = 'lg';
		}
		
		$this->class(
			'bg-' . $this->bg,
			'navbar-expand-' . $this->expand
		);
		
		$this->addColorClass('navbar', 'dark');
		
		return parent::getHtml();
	}
}
