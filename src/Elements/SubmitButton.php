<?php

namespace Lightning\View\Elements;

class SubmitButton extends ButtonBase
{
	public function __construct($children = [])
	{
		parent::__construct($children);
		$this->type('submit');
	}
	
	public function getHtml() : string
	{
		// if the name is defined but not the value, we use the name as a value
		if ($this->name && !isset($this->value)) {
			$this->value($this->name);
		}
		
		return parent::getHtml();
	}
}
