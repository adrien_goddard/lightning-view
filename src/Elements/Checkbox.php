<?php

namespace Lightning\View\Elements;

use Lightning\View\Tags\Input;

class Checkbox extends Input
{
	public static function getFillableAttributes(): array
	{
		return [
			'type' => 'checkbox',
			'checked' => null,
		];
	}
}
