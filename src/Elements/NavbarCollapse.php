<?php

namespace Lightning\View\Elements;

use Lightning\View\Tag;
use Lightning\View\TagWithFamily;
use Lightning\View\Tags\Form;

class NavbarCollapse extends TagWithFamily
{
	public function __construct($children = [])
	{
		parent::__construct();
		
		$this
			->class('collapse', 'navbar-collapse')
			->append(...$children);
	}
	
	public function parseChild($child)
	{
		// automatically add .form-inline to <form> tags
		if ($child instanceof Form) {
			$child->class('form-inline');
		}
		
		return parent::parseChild($child);
	}

	public function getToggler(): Tag
	{
		$icon = _span()->class('navbar-toggler-icon');
		
		return new Toggler($this, [$icon]);
	}
}
