<?php

namespace Lightning\View\Elements;

use Lightning\View\TagWithFamily;
use Lightning\View\Traits\HasColor;

class Alert extends TagWithFamily
{
	use HasColor;
	
	public function __construct($children = [])
	{
		parent::__construct();
		$this
			->class('alert')
			->append(...$children);
	}
	
	public function getHtml() : string
	{
		$this->addColorClass('alert');
		
		return parent::getHtml();
	}
}
