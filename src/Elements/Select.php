<?php

namespace Lightning\View\Elements;

use Lightning\View\Tags\Button;

class Select extends Dropdown
{
	protected $input;
	protected $title;
	
	public static function getAliasProperties()
	{
		return ['input'];
	}
	
	public function __construct(array $values = [])
	{
		$this->input = _input()
			->type('hidden')
			->appendTo($this)
			->setOption('container', $this);
		
		parent::__construct();
		
		$this->addValues($values);
	}
	
	public function parseMenuChild($child)
	{
		// we need at least one character to correctly display the button
		if (!strlen($child->getContent())) {
			$child->append(_html('&nbsp;'));
		}
		
		return parent::parseMenuChild($child);
	}
	
	public function getHtml(): string
	{
		if ($this->title) {
			$this->menu->class('dropdown-menu-right');
			
			$return = _inputgroup($this->title)
				->class($this->class)
				->removeClass('dropdown')
				->class('select');
			
			$parent = $this->getParent();
			if ($parent !== null) {
				// we have to define the parent before $this is appended to another tag
				$return->setParent($parent);
			}
			
			$this->title = null;
			$this->wrapped = false;
			
			$return->append(
				_div($this)->class('input-group-append')
			);
			
			return $return;
		}
		
		$this->input->fillUndefinedValueFromRequest();
		
		$values = [];
		foreach ($this->menu->getChildren() as $child) {
			$values[$child->dataValue] = $child->getChildren();
		}
		
		if ($this->input->value === null && count($values) > 0) {
			$firstKey = array_keys($values)[0];
			$this->input->value = $firstKey;
		}
		
		$valueContent = $values[$this->input->value] ?? null;
		if ($valueContent !== null) {
			$this->toggler->append(...$valueContent);
		}
		
		$this->prepareMenu();
		
		return parent::getHtml();
	}
	
	public function prepareMenu()
	{
		// add a class so Jquery can target those elements
		$this->menu->class('select-menu');
	}
	
	public function addEmptyLine(): self
	{
		$this->prepend(_button());
		return $this;
	}
	
	public function addValues(array $values): self
	{
		foreach ($values as $key => $value) {
			$this->addValue($key, $value);
		}
		
		return $this;
	}
	
	public function addValue($key, $value): self
	{
		if (!($value instanceof Button)) {
			$value = _button($value);
		}
		
		$this->append(
			$value->dataValue($key)
		);
		
		return $this;
	}
}
