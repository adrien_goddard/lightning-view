<?php

use Lightning\View\Elements\Alert;
use Lightning\View\Elements\Badge;
use Lightning\View\Elements\Button;
use Lightning\View\Elements\ButtonBase;
use Lightning\View\Elements\ButtonGroup;
use Lightning\View\Elements\Card;
use Lightning\View\Elements\CustomCheckbox;
use Lightning\View\Elements\Col;
use Lightning\View\Elements\Dropdown;
use Lightning\View\Elements\FileButton;
use Lightning\View\Elements\FileInput;
use Lightning\View\Elements\FormGroup;
use Lightning\View\Elements\InputGroup;
use Lightning\View\Elements\ListGroup;
use Lightning\View\Elements\Listing;
use Lightning\View\Elements\Navbar;
use Lightning\View\Elements\NavbarCollapse;
use Lightning\View\Elements\NavbarNav;
use Lightning\View\Elements\Select;
use Lightning\View\Elements\SubmitButton;
use Lightning\View\Elements\Tabs;
use Lightning\View\Elements\Toast;
use Lightning\View\Elements\Toggler;
use Lightning\View\Html;
use Lightning\View\Page;
use Lightning\View\TagWithFamily;
use Lightning\View\TagWithParent;
use Lightning\View\Tags\A;
use Lightning\View\Tags\Canvas;
use Lightning\View\Tags\Form;
use Lightning\View\Tags\Heading;
use Lightning\View\Tags\Iframe;
use Lightning\View\Tags\Img;
use Lightning\View\Tags\Input;
use Lightning\View\Tags\Label;
use Lightning\View\Tags\Li;
use Lightning\View\Tags\Link;
use Lightning\View\Tags\Meta;
use Lightning\View\Tags\Script;
use Lightning\View\Tags\Table;
use Lightning\View\Tags\TableRowGroup;
use Lightning\View\Tags\Textarea;
use Lightning\View\Tags\Td;
use Lightning\View\Tags\Tr;

if (!function_exists('_page')) {
	function _page(...$children): Page
	{
		return new Page($children);
	}
}

if (!function_exists('_html')) {
	function _html(...$html): Html
	{
		return new Html(...$html);
	}
}

if (!function_exists('_head')) {
	function _head(...$children): TagWithFamily
	{
		return (new TagWithFamily('head'))->append(...$children);
	}
}

if (!function_exists('_body')) {
	function _body(...$children): TagWithFamily
	{
		return (new TagWithFamily('body'))->append(...$children);
	}
}

if (!function_exists('_meta')) {
	function _meta(): Meta
	{
		return new Meta;
	}
}

if (!function_exists('_link')) {
	function _link(): Link
	{
		return new Link;
	}
}

if (!function_exists('_script')) {
	function _script(...$children): Script
	{
		return new Script($children);
	}
}

if (!function_exists('_style')) {
	function _style(...$children): TagWithFamily
	{
		return (new TagWithFamily('style'))->append(...$children);
	}
}

if (!function_exists('_title')) {
	function _title($title): TagWithFamily
	{
		return (new TagWithFamily('title'))->content($title);
	}
}

if (!function_exists('_div')) {
	function _div(...$children): TagWithFamily
	{
		return (new TagWithFamily)->append(...$children);
	}
}

if (!function_exists('_span')) {
	function _span(...$children): TagWithFamily
	{
		return (new TagWithFamily('span'))->append(...$children);
	}
}

if (!function_exists('_p')) {
	function _p(...$children): TagWithFamily
	{
		return (new TagWithFamily('p'))->append(...$children);
	}
}

if (!function_exists('_small')) {
	function _small(...$children): TagWithFamily
	{
		return (new TagWithFamily('small'))->append(...$children);
	}
}

if (!function_exists('_mark')) {
	function _mark(...$children): TagWithFamily
	{
		return (new TagWithFamily('mark'))->append(...$children);
	}
}

if (!function_exists('_del')) {
	function _del(...$children): TagWithFamily
	{
		return (new TagWithFamily('del'))->append(...$children);
	}
}

if (!function_exists('_s')) {
	function _s(...$children): TagWithFamily
	{
		return (new TagWithFamily('s'))->append(...$children);
	}
}

if (!function_exists('_ins')) {
	function _ins(...$children): TagWithFamily
	{
		return (new TagWithFamily('ins'))->append(...$children);
	}
}

if (!function_exists('_u')) {
	function _u(...$children): TagWithFamily
	{
		return (new TagWithFamily('u'))->append(...$children);
	}
}

if (!function_exists('_strong')) {
	function _strong(...$children): TagWithFamily
	{
		return (new TagWithFamily('strong'))->append(...$children);
	}
}

if (!function_exists('_em')) {
	function _em(...$children): TagWithFamily
	{
		return (new TagWithFamily('em'))->append(...$children);
	}
}

if (!function_exists('_sup')) {
	function _sup(...$children): TagWithFamily
	{
		return (new TagWithFamily('sup'))->append(...$children);
	}
}

if (!function_exists('_sub')) {
	function _sub(...$children): TagWithFamily
	{
		return (new TagWithFamily('sub'))->append(...$children);
	}
}

if (!function_exists('_a')) {
	function _a(...$children): A
	{
		return new A($children);
	}
}

if (!function_exists('_br')) {
	function _br(): TagWithParent
	{
		return new TagWithParent('br', ['self_closing' => true]);
	}
}

if (!function_exists('_hr')) {
	function _hr(): TagWithParent
	{
		return new TagWithParent('hr', ['self_closing' => true]);
	}
}

if (!function_exists('_h1')) {
	function _h1(...$children): Heading
	{
		return (new Heading($children))->setType('h1');
	}
}

if (!function_exists('_h2')) {
	function _h2(...$children): Heading
	{
		return (new Heading($children))->setType('h2');
	}
}

if (!function_exists('_h3')) {
	function _h3(...$children): Heading
	{
		return (new Heading($children))->setType('h3');
	}
}

if (!function_exists('_h4')) {
	function _h4(...$children): Heading
	{
		return (new Heading($children))->setType('h4');
	}
}

if (!function_exists('_h5')) {
	function _h5(...$children): Heading
	{
		return (new Heading($children))->setType('h5');
	}
}

if (!function_exists('_h6')) {
	function _h6(...$children): Heading
	{
		return (new Heading($children))->setType('h6');
	}
}

if (!function_exists('_button')) {
	function _button(...$children): Button
	{
		return new Button($children);
	}
}

if (!function_exists('_submitbutton')) {
	function _submitbutton(...$children): ButtonBase
	{
		return new SubmitButton($children);
	}
}

if (!function_exists('_closebutton')) {
	function _closebutton(...$children): ButtonBase
	{
		return (new ButtonBase($children))->dataDismiss('modal');
	}
}

if (!function_exists('_buttongroup')) {
	function _buttongroup(...$children): ButtonGroup
	{
		return new ButtonGroup($children);
	}
}

if (!function_exists('_header')) {
	function _header(...$children): TagWithFamily
	{
		return (new TagWithFamily('header'))->append(...$children);
	}
}

if (!function_exists('_footer')) {
	function _footer(...$children): TagWithFamily
	{
		return (new TagWithFamily('footer'))->append(...$children);
	}
}

if (!function_exists('_container')) {
	function _container(...$children): TagWithFamily
	{
		return (new TagWithFamily)
			->class('container')
			->append(...$children);
	}
}

if (!function_exists('_fluidcontainer')) {
	function _fluidcontainer(...$children): TagWithFamily
	{
		return (new TagWithFamily)
			->class('container-fluid')
			->append(...$children);
	}
}

if (!function_exists('_row')) {
	function _row(...$children): TagWithFamily
	{
		return (new TagWithFamily)
			->class('row')
			->append(...$children);
	}
}

if (!function_exists('_col')) {
	function _col(...$children): Col
	{
		return new Col($children);
	}
}

if (!function_exists('_navbar')) {
	function _navbar(...$children): Navbar
	{
		return new Navbar($children);
	}
}

if (!function_exists('_navbarbrand')) {
	function _navbarbrand(...$children): A
	{
		return (new A)
			->class('navbar-brand')
			->href('/')
			->append(...$children);
	}
}

if (!function_exists('_navbarcollapse')) {
	function _navbarcollapse(...$children): NavbarCollapse
	{
		return new NavbarCollapse($children);
	}
}

if (!function_exists('_navbarnav')) {
	function _navbarnav(...$children): NavbarNav
	{
		return new NavbarNav($children);
	}
}

if (!function_exists('_ul')) {
	function _ul(...$children): Listing
	{
		return new Listing('ul', $children);
	}
}

if (!function_exists('_ol')) {
	function _ol(...$children): Listing
	{
		return new Listing('ol', $children);
	}
}

if (!function_exists('_li')) {
	function _li(...$children): Li
	{
		return new Li(...$children);
	}
}

if (!function_exists('_listgroup')) {
	function _listgroup(...$children): ListGroup
	{
		return new ListGroup($children);
	}
}

if (!function_exists('_tabs')) {
	function _tabs(...$children): Tabs
	{
		return new Tabs($children);
	}
}

if (!function_exists('_img')) {
	function _img(?string $src = null): Img
	{
		return new Img($src);
	}
}

if (!function_exists('_form')) {
	function _form(...$children): Form
	{
		return new Form($children);
	}
}

if (!function_exists('_formgroup')) {
	function _formgroup(...$children): FormGroup
	{
		return new FormGroup($children);
	}
}

if (!function_exists('_input')) {
	function _input(): Input
	{
		return new Input;
	}
}

if (!function_exists('_textarea')) {
	function _textarea(?string $value = null): Textarea
	{
		return new Textarea($value);
	}
}

if (!function_exists('_filebutton')) {
	function _filebutton(...$children): FileButton
	{
		return new FileButton($children);
	}
}

if (!function_exists('_fileinput')) {
	function _fileinput(...$children): FileInput
	{
		return new FileInput($children);
	}
}

if (!function_exists('_inputgroup')) {
	function _inputgroup(...$children): InputGroup
	{
		return new InputGroup($children);
	}
}

if (!function_exists('_label')) {
	function _label(...$children): Label
	{
		return new Label($children);
	}
}

if (!function_exists('_dropdown')) {
	function _dropdown(...$children): Dropdown
	{
		return new Dropdown($children);
	}
}

if (!function_exists('_select')) {
	function _select(array $values = []): Select
	{
		return new Select($values);
	}
}

if (!function_exists('_alert')) {
	function _alert(...$children): Alert
	{
		return new Alert($children);
	}
}

if (!function_exists('_card')) {
	function _card(...$children): Card
	{
		return new Card($children);
	}
}

if (!function_exists('_toast')) {
	function _toast(...$children): Toast
	{
		return new Toast($children);
	}
}

if (!function_exists('_table')) {
	function _table(...$children): Table
	{
		return new Table($children);
	}
}

if (!function_exists('_thead')) {
	function _thead(...$children): TableRowGroup
	{
		return new TableRowGroup('thead', $children);
	}
}

if (!function_exists('_tbody')) {
	function _tbody(...$children): TableRowGroup
	{
		return new TableRowGroup('tbody', $children);
	}
}

if (!function_exists('_tfoot')) {
	function _tfoot(...$children): TableRowGroup
	{
		return new TableRowGroup('tfoot', $children);
	}
}

if (!function_exists('_tr')) {
	function _tr(...$children): Tr
	{
		return new Tr($children);
	}
}

if (!function_exists('_th')) {
	function _th(...$children): TagWithFamily
	{
		return (new TagWithFamily('th'))->append(...$children);
	}
}

if (!function_exists('_td')) {
	function _td(...$children): Td
	{
		return new Td($children);
	}
}

if (!function_exists('_skinnycolumn')) {
	function _skinnycolumn(...$children): TagWithFamily
	{
		return _th(...$children)->class('skinny-column');
	}
}

if (!function_exists('_badge')) {
	function _badge(...$children): Badge
	{
		return new Badge($children);
	}
}

if (!function_exists('_clearfix')) {
	function _clearfix(...$children): TagWithFamily
	{
		return (new TagWithFamily)->append(...$children)->class('clearfix');
	}
}

if (!function_exists('_checkbox')) {
	function _checkbox(): CustomCheckbox
	{
		return new CustomCheckbox;
	}
}

if (!function_exists('_iframe')) {
	function _iframe(): Iframe
	{
		return new Iframe;
	}
}

if (!function_exists('_canvas')) {
	function _canvas(...$children): TagWithFamily
	{
		return new Canvas($children);
	}
}

if (!function_exists('_toggler')) {
	function _toggler($target, ...$children): Toggler
	{
		return new Toggler($target, $children);
	}
}
