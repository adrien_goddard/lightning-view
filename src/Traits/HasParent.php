<?php

namespace Lightning\View\Traits;

use Lightning\View\Interfaces\ChildTag;
use Lightning\View\Interfaces\ParentTag;

trait HasParent
{
	private $parent;
	
	public function getParent(): ?ParentTag
	{
		return $this->parent;
	}
	
	public function getRootParent(): ?ParentTag
	{
		if ($this->parent === null) {
			return null;
		}
		
		$parent = $this->parent;
		while ($parent instanceof ChildTag && $parent->getParent() !== null) {
			$parent = $parent->getParent();
		}
		return $parent;
	}
	
	public function appendTo(ParentTag $parent): ChildTag
	{
		$parent->append($this);

		return $this;
	}
	
	public function prependTo(ParentTag $parent): ChildTag
	{
		$parent->prepend($this);

		return $this;
	}
	
	public function setParent(ParentTag $parent): ChildTag
	{
		$this->parent = $parent;
		return $this;
	}
	
	public function replaceWith($replacement): ChildTag
	{
		$children = $this->parent->getChildren();
		foreach ($children as $k => $child) {
			if ($child == $this) {
				if ($replacement === null) {
					unset($children[$k]);
				} else {
					$children[$k] = $replacement;
					
					if ($replacement instanceof ChildTag) {
						$replacement->setParent($this->parent);
					}
				}
				
				break;
			}
		}

		$this->parent->setChildren($children);

		return $this;
	}
	
	public function wrapIn(ParentTag $wrapper): ChildTag
	{
		$this->replaceWith($wrapper);
		$wrapper->append($this);
		
		return $this;
	}
	
	public function remove(): ChildTag
	{
		$this->replaceWith(null);

		return $this;
	}
	
	public function isLastChild(): bool
	{
		$parentLastChild = $this->getParent()->getLastChild();
		return $this === $parentLastChild;
	}
	
	public function isFirstChild(): bool
	{
		$parentFirstChild = $this->getParent()->getFirstChild();
		return $this === $parentFirstChild;
	}
}
