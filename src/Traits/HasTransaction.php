<?php

namespace Lightning\View\Traits;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

trait HasTransaction
{
	public $request;
	public $response;
	
	public function __construct(RequestInterface $request, ResponseInterface $response)
	{
		$this->request = $request;
		$this->response = $response;
	}
	
	public function setStatus($status)
	{
		$this->response = $this->response->withStatus($status);
	}
	
	public function addHeader($header, $value)
	{
		$this->response = $this->response->withAddedHeader($header, $value);
	}
	
	public function addResponse($response)
	{
		$this->response->getBody()->write($response);
	}
}
