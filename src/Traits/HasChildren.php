<?php

namespace Lightning\View\Traits;

use Closure;
use Lightning\View\Interfaces\ParentTag;
use Lightning\View\Tag;

trait HasChildren
{
	use HasOptions;
	
	private $children = [];
	// sometimes, we want to execute all the function to add children on a different element (ex : modal()->append(...) have to be executed on the body)
	private $childrenContainer;
	
	public function hasChildren(): bool
	{
		if ($this->childrenContainer ?? false) {
			return $this->childrenContainer->hasChildren();
		}
		
		return count($this->children) > 0;
	}
	
	public function append(...$children): ParentTag
	{
		if ($this->childrenContainer ?? false) {
			return $this->childrenContainer->append(...$children);
		}
		
		$children = $this->parseChildren($children);
		$this->children = array_merge($this->children, $children);

		return $this;
	}

	public function prepend(...$children): ParentTag
	{
		if ($this->childrenContainer ?? false) {
			return $this->childrenContainer->prepend(...$children);
		}
		
		$children = $this->parseChildren($children);
		$this->children = array_merge($children, $this->children);

		return $this;
	}

	public function content(...$children): ParentTag
	{
		// if there is a children container, we forward the new content to it
		if ($this->childrenContainer ?? false) {
			return $this->childrenContainer->content(...$children);
		}
		
		$this->children = [];

		return $this->append(...$children);
	}

	public function parseChildren(array $children): array
	{
		$return = [];

		foreach ($children as $child) {
			if (
				$child instanceof Tag
				&& isset($child->options['container'])
				&& $child->options['container'] != $this
			) {
				$child = $child->options['container'];
			}

			if (is_array($child) && !($this->options['disallow_array_child_unpacking'] ?? false)) {
				$return = array_merge($return, $this->parseChildren($child));
			} elseif ($child instanceof Closure) {
				$closureReturn = $child($this);

				if (is_array($closureReturn)) {
					$return = array_merge($return, $this->parseChildren($closureReturn));
				}
			} else {
				$return[] = $this->parseChild($child);
			}
		}

		return $return;
	}

	public function parseChild($child)
	{
		if ($this->options['child_parser'] ?? false) {
			$function = $this->options['child_parser'];
			$child = $function($this, $child);
		}
		
		// filling the parent property
		if ($child instanceof Tag) {
			$child->setParent($this);
		}

		return $child;
	}

	public function setChildrenContainer(ParentTag $childrenContainer): ParentTag
	{
		$this->childrenContainer = $childrenContainer;
		
		return $childrenContainer;
	}
	
	public function getContent(): string
	{
		$return = '';
		foreach ($this->children as $child) {
			if (is_string($child)) {
				// automatically encode html entities
				// ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401 : default value
				// $double_encode = false in order for &#127873; to be interpreted as 🎁
				$return .= htmlentities($child, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401, null, false);
			} elseif ($child instanceof Tag) {
				$return .= $child->getHtml();
			} else {
				$return .= (string)$child;
			}
		}
		
		return $return;
	}

	public function getChildren(): array
	{
		return $this->children;
	}

	public function setChildren(array $children)
	{
		return $this->children = $children;
	}

	public function getAllChildren(): array
	{
		$children = [];
		foreach ($this->children as $child) {
			$children[] = $child;
			
			if (
				$child instanceof self
				&& !empty($child->children)
			) {
				$children = array_merge($children, $child->getAllChildren());
			}
		}
		
		return $children;
	}
	
	public function getFirstChild(?Closure $callback = null)
	{
		foreach ($this->children as $child) {
			if (
				$callback === null
				|| $callback($child) === true
			) {
				return $child;
			}
		}
	}

	public function getLastChild(?Closure $callback = null)
	{
		foreach (array_reverse($this->children) as $child) {
			if (
				$callback === null
				|| $callback($child) === true
			) {
				return $child;
			}
		}
	}
}
