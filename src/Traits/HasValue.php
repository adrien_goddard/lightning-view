<?php

namespace Lightning\View\Traits;

use Lightning\View\Page;
use Psr\Http\Message\RequestInterface;

trait HasValue
{
	public $defaultValue = null;
	
	public function fillUndefinedValueFromRequest()
	{
		// if the value is undefined
		if ($this->value === null) {
			$firstParent = $this->getRootParent();
			
			// and the first parent is a Page with a Request
			if ($firstParent instanceof Page && $firstParent->request instanceof RequestInterface) {
				// we fill the input with the previously sent value
				$this->value = $firstParent->request->getParsedBody()[$this->name];
			}
		}
		
		// if the value is still undefined and there is a default value
		if ($this->value === null && $this->defaultValue !== null) {
			$this->value = $this->defaultValue;
		}
	}
}
