<?php

namespace Lightning\View\Traits;

use Lightning\View\Tag;

trait HasSize
{
	public $size;
	
	public function addSizeClass(string $prefix, ?string $default = null) : Tag
	{
		$sizeContainer = $this->getSizeContainer();

		if (isset($this->size)) {
			$sizeContainer->class($prefix . '-' . $this->size);
		} elseif ($default !== null) {
			$sizeContainer->class($default);
		}
		
		return $this;
	}

	protected function getSizeContainer(): Tag
	{
		return $this;
	}
}
