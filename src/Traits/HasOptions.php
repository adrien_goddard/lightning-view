<?php

namespace Lightning\View\Traits;

trait HasOptions
{
	public $options = [];
}
