<?php

namespace Lightning\View\Traits;

trait HasHref
{
	public static function getFillableAttributes() : array
	{
		return ['href' => null, 'target' => null];
	}
	
	public function blank() : self
	{
		return $this->target('_blank');
	}
}
