<?php

namespace Lightning\View\Traits;

trait HasColor
{
	public $color;
	
	public function addColorClass(string $prefix, ?string $default = 'primary')
	{
		// primary color by default
		if ($default !== null && !isset($this->color)) {
			$this->color = $default;
		}
		
		if (strlen($this->color) > 0) {
			$this->class($prefix . '-' . $this->color);
		}
	}
}
