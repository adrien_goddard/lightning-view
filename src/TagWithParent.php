<?php

namespace Lightning\View;

use Lightning\View\Interfaces\ChildTag;
use Lightning\View\Traits\HasParent;

class TagWithParent extends Tag implements ChildTag
{
	use HasParent;
}
