<?php

namespace Lightning\View\Tags;

use Lightning\View\TagWithFamily;

class Td extends TagWithFamily
{
	public static function getFillableAttributes(): array
	{
		return [
			'colspan' => null
		];
	}

	public function __construct(array $children = [])
	{
		parent::__construct('td');
		$this->append(...$children);
	}
}