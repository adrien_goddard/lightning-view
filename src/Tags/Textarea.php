<?php

namespace Lightning\View\Tags;

use Lightning\View\Traits\HasSize;
use Lightning\View\Traits\HasValue;

class Textarea extends InputBase
{
	use HasSize;
	use HasValue;
	
	public $value;
	
	public function __construct($value = null)
	{
		parent::__construct('textarea');
		$this->value = $value;
	}
	
	public function getHtml() : string
	{
		$this->class('form-control')
			->addSizeClass('form-control')
			->fillUndefinedValueFromRequest();
		
		return parent::getHtml();
	}
	
	public function getContent() : string
	{
		return (string)$this->value;
	}
}
