<?php

namespace Lightning\View\Tags;

use Lightning\View\Elements\Listing;
use Lightning\View\Interfaces\ChildTag;
use Lightning\View\TagWithFamily;

class Li extends TagWithFamily
{
	public function __construct(...$children)
	{
		parent::__construct('li');
		$this->append(...$children);
	}
	
	public function setParent($parent) : ChildTag
	{
		if (!($parent instanceof Listing)) {
			trigger_error('A <li> should always have a <ul> or <ol> as parent', E_USER_NOTICE);
		}
		
		return parent::setParent($parent);
	}
}
