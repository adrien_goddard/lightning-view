<?php

namespace Lightning\View\Tags;

use Lightning\View\TagWithParent;

class InputBase extends TagWithParent
{
	public static function getFillableAttributes() : array
	{
		return [
			'name' => null,
			'placeholder' => null,
			'readonly' => false,
			'disabled' => null,
		];
	}
}
