<?php

namespace Lightning\View\Tags;

use Lightning\View\TagWithFamily;

class Iframe extends TagWithFamily
{
	public function __construct()
	{
		parent::__construct('iframe');
	}
	
	public static function getFillableAttributes() : array
	{
		return [
			'src' => null,
		];
	}
}
