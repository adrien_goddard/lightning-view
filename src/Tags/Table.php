<?php

namespace Lightning\View\Tags;

use Lightning\View\Interfaces\ParentTag;
use Lightning\View\Tag;
use Lightning\View\TagWithFamily;
use Lightning\View\Tags\TableRowGroup;

class Table extends TagWithFamily
{
	protected $footer;
	protected $header;
	
	public function __construct($children = [])
	{
		parent::__construct('table');
		
		$this->setHeader(static::newThead());
		$this->setFooter(static::newTfoot());
		
		$this
			->class('table')
			->append(...$children);
	}
	
	public function getHtml() : string
	{
		$children = [];
		
		if ($this->header->hasChildren()) {
			$children[] = $this->header;
			
			$skinnyColumns = [];
			foreach ($this->header->getChildren() as $tRow) {
				foreach ($tRow->getChildren() as $k => $cell) {
					if ($cell instanceof Tag && $cell->hasClass('skinny-column')) {
						$skinnyColumns[] = $k;
					}
				}
			}
		} else {
			$skinnyColumns = null;
		}
		
		if ($skinnyColumns) {
			foreach ($this->getChildren() as $tBody) {
				foreach ($tBody->getChildren() as $tRow) {
					foreach ($tRow->getChildren() as $k => $cell) {
						if (in_array($k, $skinnyColumns)) {
							$cell->class('skinny-column');
						}
					}
				}
			}
		}
		$children = array_merge($children, $this->getChildren());
		
		if ($this->footer->hasChildren()) {
			$children[] = $this->footer;
		}
		
		$this->setChildren($children);
		
		return parent::getHtml();
	}
	
	public function header(...$children)
	{
		if (count($children) == 1) {
			$child = reset($children);
			
			if ($child instanceof Tag && $child->getType() == 'thead') {
				$this->setHeader($child);
				return $child;
			}
		}
		
		return $this->header->append(...$children);
	}
	
	public function body(...$children)
	{
		return $this->append(...$children);
	}
	
	public function footer(...$children)
	{
		if (count($children) == 1) {
			$child = reset($children);
			
			if ($child instanceof Tag && $child->getType() == 'tfoot') {
				$this->setFooter($child);
				return $child;
			}
		}
		
		return $this->footer->append(...$children);
	}
	
	/**
	 * @param mixed $children,...
	 * @return $this
	 */
	public function append(...$children) : ParentTag
	{
		if (count($children) > 0) {
			$firstValue = reset($children);
			if ($firstValue instanceof TableRowGroup) {
				parent::append(...$children);
			} else {
				// we sent a bunch of <tr>, that we need to wrap in a <tbody>
				$tBodies = $this->getTBodies();
				if (count($tBodies) == 0) {
					$tBody = static::newTbody();
					$appendTBody = true;
				} else {
					// using last tbody
					$tBody = array_pop($tBodies);
					$appendTBody = false;
				}
				
				$tBody->append(...$children);
				
				// if it's a new tbody which has children, we append it to the table
				// must be after ->append(...$children) so we know whether a closure generated any children
				if ($appendTBody && $tBody->hasChildren()) {
					parent::append($tBody);
				}
			}
		}
		
		return $this;
	}
	
	/**
	 * @param mixed $children,...
	 * @return $this
	 */
	public function prepend(...$children) : ParentTag
	{
		if (count($children) > 0) {
			$firstValue = reset($children);
			if ($firstValue instanceof TableRowGroup) {
				parent::prepend(...$children);
			} else {
				// we sent a bunch of <tr>, that we need to wrap in a <tbody>
				$tBodies = $this->getTBodies();
				if (count($tBodies) == 0) {
					$tBody = _tbody();
					parent::append($tBody);
				} else {
					// using first tbody
					$tBody = reset($tBodies);
				}
				
				$tBody->prepend(...$children);
			}
		}
		
		return $this;
	}
	
	public function responsive(?string $size = null)
	{
		$class = 'table-responsive';
		if (isset($size)) {
			$class .= '-' . $size;
		}
		
		$div = _div($this);
		$div->options['method_proxies'][] = $this;
		
		return $div->class($class);
	}
	
	private function setHeader($header)
	{
		$header->setOption('container', $this);
		$this->header = $header;
	}
	
	private function setFooter($footer)
	{
		$footer->setOption('container', $this);
		$this->footer = $footer;
	}
	
	private function getTBodies() : array
	{
		return array_filter($this->getChildren(), function ($child) {
			return $child->getType() == 'tbody';
		});
	}
	
	protected static function newThead()
	{
		return _thead();
	}
	
	protected static function newTfoot()
	{
		return _tfoot();
	}
	
	protected static function newTbody()
	{
		return _tbody();
	}
}
