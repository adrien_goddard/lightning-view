<?php

namespace Lightning\View\Tags;

use Lightning\View\Interfaces\ChildTag;
use Lightning\View\TagWithFamily;

class Label extends TagWithFamily
{
	public static function getFillableAttributes() : array
	{
		return ['for' => null];
	}
	
	public function __construct($children = [])
	{
		parent::__construct('label');
		$this->append(...$children);
	}
	
	public function setParent($parent) : ChildTag
	{
		if ($parent->hasClass('form-group') && $parent->hasClass('row')) {
			$this->class('col-form-label');
		}
		
		return parent::setParent($parent);
	}
}
