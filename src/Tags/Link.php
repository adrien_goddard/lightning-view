<?php

namespace Lightning\View\Tags;

use Lightning\View\TagWithParent;

class Link extends TagWithParent
{
	public static function getFillableAttributes() : array
	{
		return [
			'rel' => null,
			'href' => null,
		];
	}
	
	public function __construct()
	{
		parent::__construct('link', ['self_closing' => true]);
	}
}
