<?php

namespace Lightning\View\Tags;

use Lightning\View\TagWithFamily;

class Form extends TagWithFamily
{
	public static function getFillableAttributes() : array
	{
		return [
			'action' => null,
			'method' => 'post',
			'enctype' => 'multipart/form-data',
		];
	}
	
	public function __construct($children = [])
	{
		parent::__construct('form');
		$this->append(...$children);
	}
}
