<?php

namespace Lightning\View\Tags;

use Lightning\View\TagWithFamily;

class Script extends TagWithFamily
{
	public function __construct($children = [])
	{
		parent::__construct('script');
		$this->append(...$children);
	}
	
	public static function getFillableAttributes() : array
	{
		return [
			'src' => null,
			'type' => null,
		];
	}
}
