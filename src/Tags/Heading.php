<?php

namespace Lightning\View\Tags;

use Lightning\View\Elements\Badge;
use Lightning\View\Elements\ButtonBase;
use Lightning\View\TagWithFamily;

class Heading extends TagWithFamily
{
	public function __construct($children = [])
	{
		parent::__construct();
		$this->append(...$children);
	}
	
	public function getHtml() : string
	{
		// we add those classes to vertically align buttons and badges in titles
		foreach ($this->getAllChildren() as $child) {
			if ($child instanceof ButtonBase || $child instanceof Badge) {
				$this->class('d-flex', 'align-items-center');
				break;
			}
		}
		
		// we change the button size in small headings
		if (in_array($this->getType(), ['h3', 'h4', 'h5', 'h6'])) {
			foreach ($this->getChildren() as $child) {
				if ($child instanceof ButtonBase && !isset($child->size)) {
					$child->size('sm');
				}
			}
		}
		
		return parent::getHtml();
	}
}
