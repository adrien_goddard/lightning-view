<?php

namespace Lightning\View\Tags;

use Lightning\View\TagWithParent;

class Img extends TagWithParent
{
	public static function getFillableAttributes() : array
	{
		return [
			'src' => null,
			'alt' => null,
		];
	}
	
	public function __construct(?string $src = null)
	{
		parent::__construct('img', ['self_closing' => true]);
		
		if ($src !== null) {
			$this->src($src);
		}
	}
}
