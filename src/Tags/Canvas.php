<?php

namespace Lightning\View\Tags;

use Lightning\View\TagWithFamily;

class Canvas extends TagWithFamily
{
	public function __construct($children = [])
	{
		parent::__construct('canvas');
		$this->append(...$children);
	}
	
	public static function getFillableAttributes() : array
	{
		return [
			'width' => null,
			'height' => null,
		];
	}
}
