<?php

namespace Lightning\View\Tags;

use Lightning\View\TagWithFamily;

class Button extends TagWithFamily
{
	public static function getFillableAttributes() : array
	{
		return [
			'type' => 'button',
			'name' => null,
			'value' => null,
		];
	}
	
	public function __construct($children = [])
	{
		parent::__construct('button');
		$this->append(...$children);
	}
}
