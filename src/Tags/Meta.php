<?php

namespace Lightning\View\Tags;

use Lightning\View\TagWithParent;

class Meta extends TagWithParent
{
	public static function getFillableAttributes() : array
	{
		return [
			'name' => null,
			'content' => null,
			'charset' => null,
		];
	}
	
	public function __construct()
	{
		parent::__construct('meta', ['self_closing' => true]);
	}
}
