<?php

namespace Lightning\View\Tags;

use Closure;
use Lightning\View\TagWithFamily;

class TableRowGroup extends TagWithFamily
{
	public function __construct($type, $children = [])
	{
		parent::__construct($type);
		$this->append(...$children);
	}
	
	public function parseChildren(array $children) : array
	{
		if (count($children) > 0) {
			$firstValue = reset($children);
			// if the first value isn't an array, a <tr> or a closure
			if (
				!is_array($firstValue)
				&& !($firstValue instanceof Tr)
				&& !($firstValue instanceof Closure)
			) {
				// we sent a bunch of cells, that we need to wrap in a <tr>
				$children = [_tr(...$children)];
			}
		}
		
		return parent::parseChildren($children);
	}
}
