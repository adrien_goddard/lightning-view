<?php

namespace Lightning\View\Tags;

use Lightning\View\Traits\HasSize;
use Lightning\View\Traits\HasValue;

class Input extends InputBase
{
	use HasSize;
	use HasValue;
	
	public static function getFillableAttributes() : array
	{
		return [
			'type' => 'text',
			'value' => null,
			'autocomplete' => null,
			'step' => null,
		];
	}
	
	public function __construct()
	{
		parent::__construct('input', ['self_closing' => true]);
	}
	
	public function getHtml() : string
	{
		if (!in_array($this->type, ['hidden', 'file', 'checkbox'])) {
			$this->class('form-control')->addSizeClass('form-control');
		}
		
		$this->fillUndefinedValueFromRequest();
		
		return parent::getHtml();
	}
}
