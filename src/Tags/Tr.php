<?php

namespace Lightning\View\Tags;

use Lightning\View\Interfaces\ChildTag;
use Lightning\View\Tag;
use Lightning\View\TagWithFamily;

class Tr extends TagWithFamily
{
	public function __construct($children = [])
	{
		parent::__construct('tr', ['disallow_array_child_unpacking' => true]);
		$this->append(...$children);
	}
	
	public function setParent($parent) : ChildTag
	{
		$children = $this->getChildren();
		foreach ($children as $k => $child) {
			if (!($child instanceof Tag) || !in_array($child->getType(), ['th', 'td'])) {
				if (in_array($parent->getType(), ['thead', 'tfoot'])) {
					$children[$k] = _th($child);
				} else {
					$children[$k] = _td($child);
				}
			}
		}
		
		$this->setChildren($children);
		
		return parent::setParent($parent);
	}
}
