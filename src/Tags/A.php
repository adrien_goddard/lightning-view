<?php

namespace Lightning\View\Tags;

use Lightning\View\Page;
use Lightning\View\TagWithFamily;
use Lightning\View\Elements\Dropdown;
use Lightning\View\Elements\Pane;
use Lightning\View\Elements\Icon;
use Lightning\View\Traits\HasHref;
use Psr\Http\Message\RequestInterface;

class A extends TagWithFamily
{
	use HasHref;
	
	private $classIfCurrent = [];
	
	public function __construct($children = [])
	{
		parent::__construct('a');
		$this->append(...$children);
	}
	
	public function getHtml() : string
	{
		// add the classes if we are visiting the uri in the href attribute
		$firstParent = $this->getRootParent();
		if (
			$firstParent instanceof Page
			&& $firstParent->request instanceof RequestInterface
			&& $firstParent->request->getUri()->getPath() == $this->href
		) {
			$this->class($this->classIfCurrent);
		}

		Icon::addMargin($this);
		
		return parent::getHtml();
	}
	
	public function classIfCurrent($class)
	{
		$this->classIfCurrent[] = $class;
		
		return $this;
	}
	
	public function dropdown(...$children) : Dropdown
	{
		return new Dropdown($children, $this);
	}

	public function pane(...$children) : Pane
	{
		return new Pane($children, $this);
	}
}
