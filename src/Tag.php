<?php

namespace Lightning\View;

use Closure;
use Doctrine\Common\Inflector\Inflector;
use Lightning\View\Traits\HasOptions;

abstract class Tag
{
	use HasOptions;

	private $attributes = [];
	private $tooltips = [];
	private $type;

	public static function getFillableAttributes() : array
	{
		return [
			'id' => null,
			'class' => [],
			'style' => null,
		];
	}

	public function getJavascriptFillableAttributes() : array
	{
		$attributes = ['onabort', 'onafterprint', 'onbeforeprint', 'onbeforeunload', 'onblur', 'oncanplay', 'oncanplaythrough', 'onclick', 'oncontextmenu', 'oncopy', 'oncuechange', 'oncut', 'ondblclick', 'ondrag', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'ondurationchange', 'onemptied', 'onended', 'onerror', 'onfocus', 'onhashchange', 'oninput', 'oninvalid', 'onkeydown', 'onkeypress', 'onkeyup', 'onload', 'onloadeddata', 'onloadedmetadata', 'onloadstart', 'onmousedown', 'onmouseleave', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onoffline', 'ononline', 'onpagehide', 'onpageshow', 'onpaste', 'onpause', 'onplay', 'onplaying', 'onpopstate', 'onprogress', 'onratechange', 'onreset', 'onresize', 'onscroll', 'onsearch', 'onseeked', 'onseeking', 'onselect', 'onstalled', 'onstorage', 'onsubmit', 'onsuspend', 'ontimeupdate', 'ontoggle', 'onunload', 'onvolumechange', 'onwaiting', 'onwheel'];
		
		if ($this->type == 'input') {
			$attributes = array_merge($attributes, ['onchange']);
		}
		
		return $attributes;
	}
	
	public function __construct(string $type = 'div', $options = null)
	{
		$this->type = $type;
		
		if (is_array($options)) {
			$this->options = $options;
		}
	}
	
	public function __get($name)
	{
		if ($this->hasFillableAttribute($name)) {
			return $this->getFillableAttribute($name);
		} elseif (substr($name, 0, 4) == 'data') {
			// the data-* attributes are automatically accepted
			$name = str_replace('_', '-', Inflector::tableize($name));
			return $this->attributes[$name] ?? null;
		} elseif ($name == 'tooltips') {
			return $this->tooltips;
		} else {
			trigger_error("Undefined property '$name'", E_USER_NOTICE);
		}
	} // @codeCoverageIgnore
	
	public function __isset($name)
	{
		if ($this->hasFillableAttribute($name)) {
			return $this->getFillableAttribute($name) !== null;
		} elseif (substr($name, 0, 4) == 'data') {
			$name = str_replace('_', '-', Inflector::tableize($name));
			return isset($this->attributes[$name]);
		} elseif ($name == 'tooltips') {
			return true;
		} else {
			return false;
		}
	}
	
	public function __set($name, $value)
	{
		if ($this->hasFillableAttribute($name)) {
			// if the attribute is an array
			if (is_array($this->getFillableAttribute($name))) {
				$value = $this->parseArrayAttribute($name, $value);
			}
			
			$this->attributes[$name] = $value;
			return;
		}
		
		if (substr($name, 0, 4) == 'data') {
			// the data-* attributes are automatically accepted
			$name = str_replace('_', '-', Inflector::tableize($name));
			
			$this->attributes[$name] = $value;
			return;
		}
		
		if ($name == 'tooltips') {
			$this->tooltips = $this->parseArrayAttribute($name, $value);
			return;
		}
		
		// calling a property's attribute as an alias (ex : dropdown()->value)
		$aliasProperty = $this->getAliasProperty($name);
		if ($aliasProperty !== null) {
			$aliasProperty->__set($name, $value);
			return;
		}
		
		trigger_error("Undefined property '$name'", E_USER_NOTICE);
		$this->$name = $value; // @codeCoverageIgnore
	}
	
	public function __unset($name) {
		if ($this->hasFillableAttribute($name)) {
			unset($this->attributes[$name]);
		} elseif (substr($name, 0, 4) == 'data') {
			// the data-* attributes are automatically accepted
			$name = str_replace('_', '-', Inflector::tableize($name));
			
			unset($this->attributes[$name]);
		} elseif ($name == 'tooltips') {
			$this->tooltips = [];
		} else {
			trigger_error("Undefined property '$name'", E_USER_NOTICE);
		}
	}
	
	public function __call($name, $arguments) : ?self
	{
		if ($this->hasFillableAttribute($name)) {
			// if the attribute's default value is an array
			if (is_array($this->getFillableAttribute($name))) {
				$this->mergeArrayAttribute($arguments, $name);
			} else {
				$this->attributes[$name] = $this->getUniqueArgument($arguments, $name);
			}
			
			return $this;
		}
		
		// the data-* attributes are automatically accepted
		if (substr($name, 0, 4) == 'data') {
			$name = str_replace('_', '-', Inflector::tableize($name));
			
			$this->attributes[$name] = $this->getUniqueArgument($arguments, $name);
			
			return $this;
		}
		
		if ($name == 'tooltip') {
			foreach ($arguments as $value) {
				if (is_array($value)) {
					$this->tooltips = array_merge($this->tooltips, $value);
				} else {
					$this->tooltips[] = $value;
				}
			}
			return $this;
		}
		
		if (property_exists($this, $name)) {
			// if the property is a Tag (ex : modal header/body/footer)
			if ($this->$name instanceof self) {
				$tag = $this->$name;
				$tag->append(...$arguments);
				
				return $tag;
			} elseif (is_array($this->$name)) {
				$this->$name = array_merge($this->$name, $arguments);
			} else {
				$this->$name = $this->getUniqueArgument($arguments, $name);
			}
			
			return $this;
		}
		
		// calling a function of the container (ex : _button()->modal()->header()->body()) or of one of the configured proxies (ex: _table()->responsive()->header())
		$proxies = $this->options['method_proxies'] ?? [];
		if ($this->options['container'] ?? false) {
			array_unshift($proxies, $this->options['container']);
		}
		foreach ($proxies as $proxy) {
			if (is_callable([$proxy, $name])) {
				return $proxy->$name(...$arguments);
			}
		}
		
		// calling a property's function as an alias (ex : dropdown()->size())
		$aliasProperty = $this->getAliasProperty($name);
		if ($aliasProperty !== null) {
			$aliasProperty->__call($name, $arguments);
			
			return $this;
		}
		
		trigger_error('Uncaught Error: Call to undefined method ' . static::class . "::$name()", E_USER_ERROR);
	}
	
	private function getAliasProperty($aliasName) : ?self
	{
		foreach (self::getAllAliasProperties() as $propertyName) {
			$property = $this->$propertyName;
			
			if (
				$property instanceof self
				&& (
					$property->hasFillableAttribute($aliasName)
					|| property_exists($property, $aliasName)
				)
			) {
				return $property;
			}
		}
		
		return null;
	}
	
	public function __toString() : string
	{
		// if this tag has a container
		if ($this->options['container'] ?? false) {
			// return the HTML of the container
			return $this->options['container']->getHtml();
		}
		
		return $this->getHtml();
	}
	
	public function getHtml() : string
	{
		foreach ($this->getAllFillableAttributes() as $attribute => $defaultValue) {
			// if the additional attribute has not been defined yet
			if (!array_key_exists($attribute, $this->attributes) && $defaultValue !== null) {
				// we set it to its default value
				$this->attributes[$attribute] = $defaultValue;
			}
		}
		
		// tooltips
		if (count($this->tooltips)) {
			$firstParent = $this->getRootParent();
			
			if ($firstParent instanceof Page) {
				$firstParent->js['tooltip'] = "
					$('.has-tooltip').tooltip();
					$('.has-tooltip').parent()
						// tooltip on a button opening a dropdown: we hide the tooltip when we click on the button
						.on('shown.bs.dropdown', function () {
							$('.tooltip').tooltip('hide')
						})
						// we hide the tooltip when a modal is opening
						.on('show.bs.modal', function () {
							$('.tooltip').tooltip('hide');
						})
						// we hide the tooltip when a modal is hidden (BS was giving back the focus to the button)
						.on('hidden.bs.modal', function () {
							$('.tooltip').tooltip('hide');
						});
				";
			}
			
			$this->class('has-tooltip')->dataHtml('true');
			$this->attributes['title'] = $this->tooltips;
		}
		
		$attributes = '';
		foreach ($this->attributes as $attributeName => $attribute) {
			// we don't add the empty attributes
			if ($attribute === null || $attribute === '' || $attribute === [] || $attribute === false) {
				continue;
			}
			
			if ($attribute === true) {
				$attributes .= " $attributeName";
				continue;
			}
			
			if (is_array($attribute)) {
				if ($attributeName == 'class') {
					$attribute = implode(' ', $attribute);
				} elseif ($attributeName == 'title') {
					$attribute = implode(_br(), $this->encodeAttributes($attribute));
				} elseif (in_array($attributeName, $this->getJavascriptFillableAttributes())) {
					$attribute = implode(';', $this->encodeAttributes($attribute));
				} else {
					trigger_error("The '$attributeName' attribute is not supposed to be an array", E_USER_WARNING);
				}
			} else {
				$attribute = $this->encodeAttribute($attribute);
			}
			
			$attributes .= " $attributeName=\"$attribute\"";
		}
		
		$tag = $this->type;
		if ($this->options['self_closing'] ?? false) {
			return "<$tag$attributes />";
		} else {
			return "<$tag$attributes>" . $this->getContent() . "</$tag>";
		}
	}
	
	public function getUniqueArgument($arguments, $name)
	{
		if (count($arguments) > 1) {
			trigger_error("You can only assign one '$name' at a time", E_USER_ERROR);
		}
		// call without any argument on a bool attribute like readonly : true by default
		if (count($arguments) == 0 && $this->getFillableAttribute($name) === false) {
			return true;
		}
		
		return $arguments[0];
	}
	
	public function mergeArrayAttribute($arguments, $name)
	{
		foreach ($arguments as $value) {
			$value = $this->parseArrayAttribute($name, $value);

			$this->attributes[$name] = array_merge($this->attributes[$name] ?? [], $value);
		}
		
		if ($name == 'class') {
			$this->attributes[$name] = array_unique($this->attributes[$name]);
		}
	}
	
	public static function getAllAliasProperties() : array
	{
		return static::getChildClassStaticProperty('alias_properties', function() {
			$properties = [];
			
			if (method_exists(static::class, 'getAliasProperties')) {
				$properties = array_merge($properties, static::getAliasProperties());
			}
			
			// check the parent classes
			foreach (class_parents(static::class) as $parentClass) {
				if (method_exists($parentClass, 'getAliasProperties')) {
					$properties = array_merge($properties, $parentClass::getAliasProperties());
				}
			}
			
			// check the traits
			foreach (class_uses_deep(static::class) as $trait) {
				if (method_exists($trait, 'getAliasProperties')) {
					$properties = array_merge($properties, $trait::getAliasProperties());
				}
			}
			
			return $properties;
		});
	}
	
	public static function getAllFillableAttributes() : array
	{
		return static::getChildClassStaticProperty('fillable_attributes', function() {
			$attributes = [];
			
			// check the parent classes
			foreach (class_parents(static::class) as $parentClass) {
				if (method_exists($parentClass, 'getFillableAttributes')) {
					$attributes = array_merge($attributes, $parentClass::getFillableAttributes());
				}
			}
			
			// check the traits
			foreach (class_uses_deep(static::class) as $trait) {
				if (method_exists($trait, 'getFillableAttributes')) {
					$attributes = array_merge($attributes, $trait::getFillableAttributes());
				}
			}
			
			return array_merge($attributes, static::getFillableAttributes());
		});
	}
	
	public function getFillableAttribute($attribute)
	{
		if (isset($this->attributes[$attribute])) {
			return $this->attributes[$attribute];
		}
		if (in_array($attribute, $this->getJavascriptFillableAttributes())) {
			return [];
		}
		return $this->getAllFillableAttributes()[$attribute];
	}
	
	public function hasFillableAttribute($attribute) : bool
	{
		return array_key_exists($attribute, $this->getAllFillableAttributes()) || in_array($attribute, $this->getJavascriptFillableAttributes());
	}
	
	public function encodeAttribute($attribute) : string
	{
		return htmlentities($attribute, ENT_QUOTES);
	}
	
	public function encodeAttributes($attributes) : array
	{
		return array_map([$this, 'encodeAttribute'], $attributes);
	}
	
	public function getType() : string
	{
		return $this->type;
	}
	
	public function setType($type) : self
	{
		$this->type = $type;
		return $this;
	}
	
	public function setOption($option, $value) : self
	{
		$this->options[$option] = $value;
		return $this;
	}
	
	public function hasClass($class) : bool
	{
		return in_array($class, $this->class);
	}
	
	public function removeClass($class)
	{
		$this->class = array_diff($this->class, [$class]);
		return $this;
	}
	
	public function parseArrayAttribute($name, $value) : array
	{
		if (!is_array($value)) {
			if ($name == 'class') {
				return explode(' ', $value);
			} else {
				return [$value];
			}
		}
		
		return $value;
	}

	public function exec($closure) : self
	{
		$closure($this);
		
		return $this;
	}
	
	// child-specific static properties
	protected static $inherited_static_properties;
	
	/**
	 * Get a static property from an inherited class.
	 * 
	 * @param  string  $propertyName
	 * @param  Closure  $callback
	 * @return mixed
	 */
	protected static function &getChildClassStaticProperty(string $propertyName, Closure $callback = null)
	{
		$property = &self::$inherited_static_properties[static::class][$propertyName];
		
		// if the property is not defined yet
		if ($property === null && $callback) {
			$property = $callback();
		}
		
		return $property;
	}
}
