<?php

namespace Lightning\View;

use Lightning\View\Traits\HasTransaction;
use Psr\Http\Message\ResponseInterface;

class Page extends TagWithChildren
{
	use HasTransaction;
	
	protected $head;
	protected $body;
	
	public $js = [];
	private $resources = [];
	
	public $charset = 'utf-8';
	public $viewport = 'width=device-width, initial-scale=1, shrink-to-fit=no';
	public $title = '';
	
	public static function getFillableAttributes() : array
	{
		return [
			'lang' => null,
		];
	}
	
	public function __construct(...$children)
	{
		parent::__construct('html');
		
		$this->head = _head()
			->appendTo($this)
			->setOption('container', $this);
		
		$this->body = _body()
			->appendTo($this)
			->setOption('container', $this);
		
		$this->setChildrenContainer($this->body)
			->append(...$children)
			->lang('en');
	}
	
	public function getHtml() : string
	{
		$this->head()->append(
			_meta()->charset($this->charset),
			_meta()->name('viewport')->content($this->viewport)
		);
		
		foreach ($this->resources as $resource) {
			if (substr($resource, -4) != '.css') {
				continue;
			}
			
			$this->head()->append(
				_link()->rel('stylesheet')->href($resource)
			);
		}
		
		if ($this->title) {
			$this->head()->append(
				_title($this->title)
			);
		}
		
		return '<!doctype html>' . parent::getHtml();
	}
	
	public function getContent() : string
	{
		// resolve the content to HTML
		$content = parent::getContent();
		
		$content .= $this->getJSTags();
		
		// if resolving the content added some JS
		if (!empty($this->js)) {
			$content .= '<script type="text/javascript">' . implode($this->js) . '</script>';
		}
		
		return $content;
	}
	
	public function getJSTags() : string
	{
		$return = '';
		foreach ($this->resources as $resource) {
			if (
				str_ends_with($resource, '.js')
				|| str_ends_with($resource, '.mjs')
			) {
				$return .= '<script src="' . $resource . '"></script>';
			}
		}
		return $return;
	}
	
	public function addResource($resource)
	{
		$this->resources[$resource] = $resource;
	}
	
	public function getResources() : array
	{
		return $this->resources;
	}
	
	public function writeResponseBody()
	{
		if ($this->response instanceof ResponseInterface) {
			// add the Content-Type header
			$this->response = $this->response->withAddedHeader('Content-Type', 'text/html; charset='.$this->charset);

			$this->response->getBody()->write((string)$this);
		}
	}
}
