<?php

namespace Lightning\View\Interfaces;

use Closure;

interface ParentTag
{
	public function hasChildren() : bool;
	
	public function append(...$children) : self;

	public function prepend(...$children) : self;

	public function content(...$children) : self;

	public function getContent() : string;

	public function getAllChildren() : array;
	
	public function getFirstChild(?Closure $callback = null);

	public function getLastChild(?Closure $callback = null);
}
