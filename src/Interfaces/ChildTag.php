<?php

namespace Lightning\View\Interfaces;

interface ChildTag
{
	public function getParent() : ?ParentTag;
	
	public function getRootParent() : ?ParentTag;
	
	public function appendTo(ParentTag $parent) : self;
	
	public function prependTo(ParentTag $parent) : self;
	
	public function setParent(ParentTag $parent) : self;
	
	public function replaceWith(self $replacement) : self;
	
	public function wrapIn(ParentTag $wrapper) : self;
	
	public function remove() : self;
	
	public function isLastChild() : bool;
}
