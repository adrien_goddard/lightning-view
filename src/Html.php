<?php

namespace Lightning\View;

class Html
{
	private $html;
	
	public function __construct(...$html)
	{
		$this->html = $html;
	}
	
	public function __toString()
	{
		return implode($this->html);
	}
}
