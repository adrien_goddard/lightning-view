<?php

namespace Lightning\View;

/*
 * From https://www.php.net/manual/fr/function.class-uses.php
 */
function class_uses_deep($class, $autoload = true) {
	$traits = [];
	do {
		$traits = array_merge(class_uses($class, $autoload), $traits);
	} while($class = get_parent_class($class));
	foreach ($traits as $trait => $same) {
		$traits = array_merge(class_uses($trait, $autoload), $traits);
	}
	return array_unique($traits);
}
