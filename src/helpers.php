<?php

function interpose($separator, array $array)
{
	$return = [];
	foreach ($array as $element) {
		$return[] = $separator;
		$return[] = $element;
	}
	array_shift($return);
	return $return;
}
