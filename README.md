# Lightning View

Lightning View is a library to generate **HTML views** in PHP, an alternative to templating languages.

![Latest Stable Version](https://img.shields.io/packagist/v/lightning/view.svg?style=flat-square)
![Total Downloads](https://img.shields.io/packagist/dt/lightning/view.svg?style=flat-square)
![License](https://img.shields.io/packagist/l/lightning/view.svg?style=flat-square)
![Build Status](https://img.shields.io/scrutinizer/build/b/adrien_goddard/lightning-view.svg?style=flat-square)
![Code Coverage](https://img.shields.io/scrutinizer/coverage/b/adrien_goddard/lightning-view.svg?style=flat-square)
![Scrutinizer Code Quality](https://img.shields.io/scrutinizer/b/adrien_goddard/lightning-view.svg?style=flat-square)

## Example

You can write this PHP code :

```php
<?php

echo _container(
	_h1('Hello world'),

	_row()->class('text-center', 'mb-4')->content(
		_col('Row 1'),
		_col('Row 2')->size(6),
		_col('Row 3')
	),

	_inputgroup(
		_input(),
		_button('Button')
	)->class('mb-4'),

	_button('Button with tooltip')->tooltip('Tooltip content'),

	_button('Button with modal')
		->class('ml-4')
		->modal()
			->header('Modal title')
			->body('Modal content')
			->footer(_closebutton('Close')),

	_buttonGroup(
		_button('Button group'),
		_button('With dropdown')->dropdown(
			_button('Button 1'),
			_button('Button 2')
		)
	)->class('ml-4')
);
```

Instead of writing this HTML code :

```html
<div class="container">
	<h1>Hello world</h1>

	<div class="row text-center mb-4">
		<div class="col">Row 1</div>
		<div class="col-6">Row 2</div>
		<div class="col">Row 3</div>
	</div>

	<div class="input-group mb-4">
		<input class="form-control" type="text" />
		<div class="input-group-append">
			<button class="btn btn-primary" type="button">Button</button>
		</div>
	</div>

	<button type="button" class="mb-4 btn btn-primary" data-toggle="tooltip" title="Tooltip content">
		Button with tooltip
	</button>

	<button class="btn btn-primary ml-4" data-toggle="modal" data-target="#modal-1" type="button">
		Button with modal
	</button>
	<div class="modal fade" id="modal-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Modal title</h5>
					<button class="close" data-dismiss="modal" type="button"><span>×</span></button>
				</div>
				<div class="modal-body">Modal content</div>
				<div class="modal-footer">
					<button class="btn btn-primary" data-dismiss="modal" type="button">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div class="btn-group ml-4">
		<button class="btn btn-primary" type="button">Button group</button>
		<button class="btn dropdown-toggle btn-primary" data-toggle="dropdown" type="button">With dropdown </button>
		<div class="dropdown-menu">
			<button class="dropdown-item" type="button">Button 1</button>
			<button class="dropdown-item" type="button">Button 2</button>
		</div>
	</div>
</div>
```

## Using elements

You can create an element simply by calling a function with it's name :

```php
<?php

$div = _div();
$span = _span();

echo $div . $span; // <div></div><span></span>
```

## Using properties

You can access HTML properties of an element with it's PHP properties or using functions :

```php
<?php

$div = _div();

// Adding some classes
$div->class('bg-primary', 'text-white');

// Replacing the classes
$div->class = 'bg-secondary text-primary';

// Getting the classes (returns an array with all the classes)
dump($div->class); // ['bg-secondary', 'text-primary']
```

## Adding children

There are two ways to add children to an element :

```php
<?php

// Sending parameters to the constructor
echo _row(
	_col('Text 1'),
	_col('Text 2')
);

// Using a function : append(), prepend() or content() (to replace all the content)
echo _row()->content(
	_col('Text 2'),
	_col('Text 3')
)->append(
	_col('Text 4')
)->prepend(
	_col('Text 1')
);
```

Both methods accept many types of arguments.

```php
<?php

// String
echo _div('Content');

// Object
echo _div(_span());

// Array
echo _div([_span(), _span()]);

// Mixed
echo _div(_span(), 'Content', [_span(), _span()]);
```

## Closures

You can use closures to interact with an element.

```php
<?php

// $row contains the row
echo _row(function ($row) use ($products) {
	foreach ($products as $product) {
		$row->append(
			_col($product->title)
		);
	}
});

// You can also use the exec() function
echo _row()->exec(function ($row) use ($products) {
	// ...
});
```

## HTML entities

All the strings you pass to the functions will automatically have their HTML entities encoded.  
If you want to send an HTML string and avoid this encoding, you can use the `_html()` function.

```php
<?php

echo _div('<span></span>');  // <div>&lt;span&gt&lt;/span&gt</div>;
echo _div(_html('<span></span>'));  // <div><span></span></div>
```

---

# Elements

## Link

You can use the `blank()` function to open a link in a new tab

```php
<?php

_a('Link')->href('/link')->blank();
```

To get :

```html
<a href="/link" target="_blank">Link</a>
```

## Button

Buttons have the primary color by default.  
You can use the `color()` and `size()` functions to customize them.

```php
<?php

_button('Button')->color('secondary')->size('sm')->onclick('doSomething()');
```

To get :

```html
<button class="btn btn-secondary btn-sm" onclick="doSomething()" type="button">Button</button>
```

## Button with link

A &lt;button&gt; tag will automatically change to a &lt;a&gt; tag if you define an href.

```php
<?php

_button('Button')->href('/link');
```

To get :

```html
<a class="btn btn-primary" href="/link">Button</a>
```

## ButtonGroup

```php
<?php

_buttonGroup(
	_button('Button 1'),
	_button('Button 2')->color('info'),
	_button('Button 3')->color('secondary')
);
```

## InputGroup

```php
<?php

_inputgroup(
	_input(),
	_button('Button')
);
```

## Dropdown

```php
<?php

_button('Dropdown')->dropdown(
	_h6('Header')
	'Simple text',
	_a('Link'),
	_hr(),
	_button('Button'),
);
```

### Dropdown in other elements

You can easily use dropdowns in other elements.

```php
<?php

_buttonGroup(
	_button('Dropdown')->dropdown(
		_button('Button 1'),
		_button('Button 2')
	)
);

_inputgroup(
	_input(),
	_button('Dropdown')->dropdown(
		_button('Button 1'),
		_button('Button 2')
	)
);

_navbarnav(
	_a('Dropdown')->dropdown(
		_a('Link 1'),
		_a('Link 2')
	)
);
```

## Select

The `select()` function displays a dropdown, with a title before, and a `<input type="hidden" />` tag to send the selected value in a form.

```php
<?php

_select([
	't' => 'Top',
	'b' => 'Bottom',
	'l' => 'Left',
	'r' => 'Right',
])->name('position')->title('Choose a position');
```

## Modal

Calling the `modal()` function on a button returns a modal which will be triggered by this button.  
You can call the `header()` and `footer()` functions to customize those parts of the modal.

```php
<?php

_button('Button with modal')
	->modal()
		->header('Modal title')
		->body('Modal content')
		->footer(_closebutton('Close'));
```

## License

The MIT License (MIT). Please see [License File](https://bitbucket.org/adrien_goddard/lightning-orm/src/master/LICENSE) for more information.
